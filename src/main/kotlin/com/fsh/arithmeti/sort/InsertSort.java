package com.fsh.arithmeti.sort;

public class InsertSort {

    public static void sort(int[] array){
        int len = array.length;
        int currentV,preIndex;
        for(int i=1;i<len;i++){
            currentV = array[i];
            preIndex = i-1;
            while (preIndex >=0 && array[preIndex] > currentV){
                array[preIndex+1] = array[preIndex];
                preIndex--;
            }
            array[preIndex+1] = currentV;
        }
    }

    public static void main(String[] args) {
        int[] a = {10,9,8,7,6,5,4,3,2,1,0};
        sort(a);
        for(int v : a){
            System.out.print(v+" ");
        }
    }
}
