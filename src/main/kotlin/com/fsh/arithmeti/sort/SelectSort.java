package com.fsh.arithmeti.sort;

public class SelectSort {

    public static void sort(int[] array){
        int minIndex;
        int len = array.length;
        for(int i=0;i<len-1;i++){
            minIndex = i;
            for(int j=i+1;j<len;j++){
                if(array[j] < array[minIndex]){
                    minIndex = j;
                }
            }
            if(i != minIndex){
                int tempV = array[minIndex];
                array[minIndex] = array[i];
                array[i] = tempV;
            }

        }
    }

    public static void main(String[] args) {
        int[] array = {10,9,8,7,6,5,4,3,2,1,0};
        sort(array);
        for(int i : array){
            System.out.print(i+" ");
        }
    }
}
