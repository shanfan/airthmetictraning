package com.fsh.arithmeti.sort;

import java.util.Arrays;

public class CountSort {

    public static void sort(int[] array){
        sort(array,getMaxValue(array));
    }

    private static int getMaxValue(int[] array){
        int max = array[0];
        for(int i=1;i<array.length;i++){
            if(array[i] > max){
                max = array[i];
            }
        }
        return max;
    }

    private static void sort(int[] array,int maxV){
        int[] bucket = new int[maxV+1];
        Arrays.fill(bucket,Integer.MIN_VALUE);
        for(int i=0;i<array.length;i++){
            if(bucket[array[i]] == Integer.MIN_VALUE){
                bucket[array[i]] = array[i];
            }else{
                bucket[array[i]] += array[i];
            }

        }
        int index =0;
        for(int i=0;i<bucket.length;i++){
            while(bucket[i] != Integer.MIN_VALUE && bucket[i] > 0){
                bucket[i] -= i;
                array[index++] = i;
            }
        }
    }

    public static void main(String[] args){
        int[] a = {10,9,8,7,6,5,4,3,2,1,18,12,4};
        sort(a);
        for(int v : a){
            System.out.print(v+" ");
        }
    }
}
