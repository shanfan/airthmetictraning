package com.fsh.arithmeti.sort;

/**
 * 冒泡排序
 * 稳定排序
 * 时间复杂度O(n^2)
 * 空间复杂度O(1)
 */
public class BubbleSort {

    public static void sort(int[] array){
        for(int i=0;i<array.length;i++){
            //优化 设定一个标记，如果未true，表示此次循环没有进行交换，也就是排好顺序了
            boolean flag = true;
            for(int j=0;j<array.length-i-1;j++){
                if(array[j] > array[j+1]){
                    int tmp = array[j];
                    array[j] = array[j+1];
                    array[j+1] = tmp;
                    flag = false;
                }
            }
            if(flag){
                break;
            }
        }
    }

    public static void main(String[] args) {
        int[] array = new int[10];
        for(int i=10;i>=1;i--){
            array[i-1] =i;
        }
        sort(array);
        for(int i : array){
            System.out.print(i+" ");
        }
    }
}
