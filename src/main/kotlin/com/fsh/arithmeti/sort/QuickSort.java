package com.fsh.arithmeti.sort;

public class QuickSort {

    private static void sort(int[] array,int left,int right){
        if(left> right){
            return;
        }
        int l = left;
        int r = right;
        int index = array[l];
        while (l < r){
            while (l<r && array[r] > index){
                r--;
            }
            if(l < r){
                array[l++] = array[r];
            }
            while (l < r && array[l] < index){
                 l++;
            }
            if(l <r){
                array[r--] = array[l];
            }
        }
        array[l] = index;
        sort(array,left,l-1);
        sort(array,l+1,right);
    }

    public static void sort(int[] array){
        sort(array,0,array.length-1);
    }

    public static void main(String[] args) {
        int[] array = {10,9,8,7,6,5,4,3,2,1,0};
        sort(array);
        for(int i: array){
            System.out.print(i+" ");
        }
    }
}
