package com.fsh.arithmeti.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MatrixRotation {

    static int[][] arr = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
    public static void main(String[] args) {
        new MatrixRotation().printRotation(arr);
    }

    private void printRotation(int[][] arr){
        List<Integer> r = roration(arr);
        System.out.println(r);
    }

    public List<Integer> roration(int[][] arr){
        int m = arr.length;
        if(m == 0) return new ArrayList<>(0);
        int n = arr[0].length;
        List<Integer> list = new ArrayList();
        int left = 0;
        int top = 0;
        int right = n-1;
        int bottom = m-1;
        while(true){
            for(int i=left;i<=right;i++){
                list.add(arr[top][i]);
            }
            if(++top > bottom)
                break;
            for(int i=top;i<=bottom;i++){
                list.add(arr[i][right]);
            }
            if(--right < left)
                break;
            for(int i=right;i>=left;i--){
                list.add(arr[bottom][i]);
            }
            if(--bottom<top)
                break;
            for(int i=bottom;i>=top;i--){
                list.add(arr[i][left]);
            }
            if(++left > right)
                break;
        }
        return list;
    }
}
