package com.fsh.arithmeti.test;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ThreadPrint {
    private static final int PRINT_COUNT = 10;
    private static ReentrantLock lock = new ReentrantLock();
    public static void main(String[] args) {

        Condition cA = lock.newCondition();
        Condition cB = lock.newCondition();
        Condition cC = lock.newCondition();
        Thread tA = new Thread(new Print(cA,cB,'A',PRINT_COUNT));
        Thread tB = new Thread(new Print(cB,cC,'B',PRINT_COUNT));
        Thread tC = new Thread(new Print(cC,cA,'C',PRINT_COUNT));
        tA.start();
        tB.start();
        tC.start();
    }

    private static class Print implements Runnable{
        Condition curr;
        Condition next;
        char printChar;
        final int printCount;
        public Print(Condition curr, Condition next, char printChar,int printCount) {
            this.curr = curr;
            this.next = next;
            this.printChar = printChar;
            this.printCount = printCount;
        }

        @Override
        public void run() {
            lock.lock();
            try{
                for(int i=0;i<printCount;i++){
                    System.out.print(printChar);
                    next.signal();
                    try {
                        curr.await();
                    } catch (InterruptedException e) {
                    }
                }
            }finally {
                lock.unlock();
            }

        }
    }
}
