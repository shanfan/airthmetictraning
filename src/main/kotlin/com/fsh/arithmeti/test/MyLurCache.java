package com.fsh.arithmeti.test;

import java.util.LinkedHashMap;

public class MyLurCache {
    private static final int DEFAULT_MAX_CACHE = 10;
    private int mMaxSize;
    private LinkedHashMap<Integer,Integer> cacge = new LinkedHashMap<>();
    public MyLurCache(int maxSize) {
        this.mMaxSize = maxSize;
        if(maxSize <= 0 || maxSize>= Integer.MAX_VALUE){
            mMaxSize = DEFAULT_MAX_CACHE;
        }
    }

    public void put(Integer key,Integer value){
        if(cacge.containsKey(key)){
            cacge.remove(key);
        }else if(cacge.size() == mMaxSize){
            Integer firstKey = cacge.keySet().iterator().next();
            cacge.remove(firstKey);
        }
        cacge.put(key,value);
    }

    public int get(int key){
        if(cacge.containsKey(key)){
            return cacge.get(key);
        }
        return -1;
    }
}
