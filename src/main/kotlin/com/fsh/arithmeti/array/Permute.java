package com.fsh.arithmeti.array;

import java.util.ArrayList;
import java.util.List;

public class Permute {

    public static List<List<Integer>> permute(int[] nums){

        List<List<Integer>> result = new ArrayList<>();
        if(nums == null || nums.length == 0){
            return result;
        }
        List<Integer> path = new ArrayList<>();
        boolean[] used = new boolean[nums.length];
        dfs(path,result,nums,0,nums.length,used);
        return result;
    }

    private static void dfs(List<Integer> path,List<List<Integer>> res,int[] nums,int deep,int len,boolean[] used){
        if(deep == len){
            res.add(new ArrayList<>(path));
            return;
        }
        for(int i=0;i<len;i++){
            if(!used[i]){
                used[i] = true;
                path.add(nums[i]);
                dfs(path,res,nums,deep+1,len,used);
                used[i] = false;
                path.remove(path.size()-1);
            }
        }
    }

    public static void main(String[] args) {
        int[] nums = {1,2,3};
        System.out.println(permute(nums).size());
    }
}
