package com.fsh.arithmeti.array;

import java.util.ArrayList;
import java.util.List;

public class Permutation {
    public String[] permutation(String S) {

        char[] array = S.toCharArray();
        List<String> list = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        dfs(list,array,sb);
        String[] res = new String[list.size()];
        list.toArray(res);
        return res;
    }

    private void dfs(List<String> res,char[] array,StringBuilder sb){
        if(sb.length() == array.length){
            res.add(sb.toString());
            return;
        }
        for(int i=0;i<array.length;i++){
            char c = array[i];
            if(array[i] != '0'){
                sb.append(array[i]);
                array[i] = '0';
                dfs(res,array,sb);
                array[i] = c;
                sb.deleteCharAt(sb.length()-1);
            }
        }
    }

    public static void main(String[] args) {
        String S = "qwe";
        String[] res = new Permutation().permutation(S);
        for(String s : res){
            System.out.println(s);
        }
    }
}
