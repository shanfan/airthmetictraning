package com.fsh.arithmeti.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProcessQueries {

    public int[] processQuries(int[] queries,int m){
        List<Integer> P = new ArrayList<>(m);
        for(int i=0;i<m;i++){
            P.add(i+1);
        }
        for(int i=0;i<queries.length;i++){
            int index = P.indexOf(queries[i]);
            queries[i] = index;
            Integer value = P.remove(index);
            P.add(0,value);
        }
        return queries;
    }

    public static void main(String[] args) {
        ProcessQueries pq = new ProcessQueries();
        int[] queries = {7,5,5,8,3};
        int m = 8;
        int[] results = pq.processQuries(queries, m);
        StringBuilder sb = new StringBuilder("[");
        for(int i=0;i<results.length;i++){
            sb.append(results[i]);
            if(i != results.length-1){
                sb.append(',');
            }else{
                sb.append(']');
            }
        }
        System.out.println(sb);
    }
}
