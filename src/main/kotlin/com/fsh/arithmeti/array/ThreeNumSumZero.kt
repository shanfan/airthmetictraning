package com.fsh.arithmeti.array

/**
 * 三数之和
 * 给你一个包含 n 个整数的数组 nums，判断 nums 中是否存在三个元素 a，b，c ，使得 a + b + c = 0 ？请你找出所有满足条件且不重复的三元组。
 *
 * 注意：答案中不可以包含重复的三元组。
 *
 *   示例：
 *       给定数组 nums = [-1, 0, 1, 2, -1, -4]，
 *       满足要求的三元组集合为：
 *
 *          [
 *
 *           [-1, 0, 1],
 *           [-1, -1, 2]
 *
 *          ]
 *
 *
 * 链接：https://leetcode-cn.com/problems/3sum
 */
object ThreeNumSumZero {
    /**
     * 解体思路：采用双指针
     *
     * 索引[index1]读取第一个目标数，然后采用两个指针[left]和[right]来查找另外两个目标数，
     * 当三个目标数相加为0，就加入结果集合[result]。
     *
     * 去重：如果当前索引的值和上个位置的值相对，就是重复的，需要跳过
     */
    fun threeSum(nums:IntArray):List<List<Int>>{
        nums.sort()
        val result = ArrayList<ArrayList<Int>>()
        for(index1 in nums.indices){
            if(nums[index1] > 0){
                break
            }
            val target = 0 - nums[index1]
            var left = index1+1
            var right = nums.size-1
            if(index1 == 0 || nums[index1] != nums[index1-1]){
                while (left < right){
                    when{
                        nums[left]+ nums[right] == target ->{
                            result.add(arrayListOf(nums[index1],nums[left],nums[right]))
                            while (left < right && nums[left] == nums[left+1]) left++
                            while (left < right && nums[right] == nums[right]) right--
                            left++
                            right--
                        }
                        nums[left] + nums[right] < target -> left++
                        nums[left] + nums[right] > target -> right--
                    }
                }
            }
        }

        return result
    }
}

fun main() {
    val nums = intArrayOf(-6,-5,-4,-4,-3,-2,-1,0,1,1,2,3,4,5,6,7)
    val startTime = System.nanoTime()
    val threeSum = ThreeNumSumZero.threeSum(nums)
    val endTime = System.nanoTime()
    println("result--->{")
    threeSum.forEach{
        println("\t$it")
    }
    println("}")
    //耗时:64110300
    //耗时:71491600
    println("耗时:${endTime - startTime}")
}