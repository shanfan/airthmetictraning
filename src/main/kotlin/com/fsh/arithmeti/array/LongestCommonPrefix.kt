package com.fsh.arithmeti.array


/**
 * 题目： 编写一个函数来查找字符串数组中的最长公共前缀。如果不存在公共前缀，则返回""
 * 示例1:
 *     输入: ["flower","flow","flight"]
 *     输出: "fl"
 * 示例 2:
 *     输入: ["dog","racecar","car"]
 *     输出: ""
 * 解释:
 *   输入不存在公共前缀。
 * 说明：
 *   所有输入只包含小写字母 a-z
 *
 *解析：
 * @see {https://www.geekxh.com/1.0.%E6%95%B0%E7%BB%84%E7%B3%BB%E5%88%97/002.html#_02%E3%80%81%E9%A2%98%E8%A7%A3%E5%88%86%E6%9E%90}
 *
 */
object LongestCommonPrefix{
    /**
     * 依次将基准元素和后面的元素进行比较（假定后面的元素依次为x1,x2,x3....），
     * 不断更新基准元素（在原基准上删除一个尾部字符），直到基准元素和所有元素都满足最长公共前缀的条件
     */
    fun commonPrefix(array:Array<String>):String{
        if(array.isEmpty()){
            return ""
        }
        var prefix = array[0]
        array.forEach {value->
            while (value.indexOf(prefix) != 0){
                if(prefix.isEmpty()){
                    return ""
                }
                prefix = prefix.substring(0,prefix.length-1)
                println("prefix --> $prefix")
            }
        }
        return prefix
    }
}

fun main(args:Array<String>){
    val array = arrayOf("flight","flipkart","flink","flipgrid","fliqlo")
    val result = LongestCommonPrefix.commonPrefix(array)
    println("LongestCommonPrefix --> $result")
}