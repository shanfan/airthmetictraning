package com.fsh.arithmeti.array

/**
 * 题目：给定一个数组，将数组中的元素向右移动 k 个位置，其中 k 是非负数。

示例 1:

输入: [1,2,3,4,5,6,7] 和 k = 3
输出: [5,6,7,1,2,3,4]
解释:
向右旋转 1 步: [7,1,2,3,4,5,6]
向右旋转 2 步: [6,7,1,2,3,4,5]
向右旋转 3 步: [5,6,7,1,2,3,4]
示例 2:

输入: [-1,-100,3,99] 和 k = 2
输出: [3,99,-1,-100]
解释:
向右旋转 1 步: [99,-1,-100,3]
向右旋转 2 步: [3,99,-1,-100]
说明:

尽可能想出更多的解决方案，至少有三种不同的方法可以解决这个问题。
要求使用空间复杂度为 O(1) 的 原地 算法。

链接：https://leetcode-cn.com/problems/rotate-array
 */
object RotationArray{
    /**
     * 第一种，双重循环，将最后一位取出，然后整体数据往后移动，然后再将取出的数据保存到第一位
     */
    fun rotate1(array:IntArray,k:Int){
        var end = k%array.size
        for(index in 0 until end){
            val temp = array.last()
            for(j in array.size-1 downTo 1){
                array[j] = array[j-1]
            }
            array[0] = temp
        }
    }

    /**
     * 第二种：
     * 1.翻转整个数组
     * 2.翻转0 --> array.size-k部分的数组
     * 3.翻转array.size-k -- array.size部分的数组
     */
    fun rotate2(array: IntArray,k:Int){
        val end = k % array.size
        reverse(array,0,array.size-1)
        reverse(array,0,end-1)
        reverse(array,end,array.size-1)
    }

    private fun reverse(array: IntArray,start:Int,end:Int){
        var star = start
        var en = end
        while (star < en){
            val temp = array[star]
            array[star++] = array[en]
            array[en--] = temp
        }
    }


}

fun main(args: Array<String>) {
    val intArray = intArrayOf(8,9,7,1,2,3)
    val k = 3
    RotationArray.rotate1(intArray,k)
    //[1, 2, 3, 8, 9, 7]
    println("rotate--> ${intArray.toList()}")
    RotationArray.rotate2(intArray,k)
    println("rotate--> ${intArray.toList()}")
}