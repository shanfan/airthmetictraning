package com.fsh.arithmeti.array

/**
 * 给定一个数组 nums，编写一个函数将所有 0 移动到数组的末尾，同时保持非零元素的相对顺序。

示例:

输入: [0,1,0,3,12]
输出: [1,3,12,0,0]
说明:

必须在原数组上操作，不能拷贝额外的数组。
尽量减少操作次数。

链接：https://leetcode-cn.com/problems/move-zeroes
 */
object MoveZeroNumInArray {
    /**
     * 方法1，暴力解决，先将不为零的数据移动到前面来，然后再将剩余的空间填充为0
     */
    fun move1(nums:IntArray){
        var i = 0
        for(index in nums.indices){
            if(nums[index] != 0){
                nums[i] = nums[index]
                i++
            }
        }
        for(index in i until nums.size){
            nums[index] = 0
        }
    }
    /**
     * 方案2：双指针
     * 快指针读取数组数据，满指针存储数据
     * 当快指针数据不为0时，就存储到慢指针的位置，将快指针的数据置为0
     * 一个字“妙”
     */
    fun move2(nums: IntArray){
        var index2 = 0
        for(index in nums.indices){
            if(nums[index] != 0){
                nums[index2] = nums[index]
                nums[index] = 0
                index2++
            }
        }
    }
}

fun main(args: Array<String>) {
    val nums = intArrayOf(0,1,0,3,12,0,1)
    MoveZeroNumInArray.move1(nums)
    println("result -> ${nums.toList()}")
    val nums2 = intArrayOf(0,1,0,3,12,0,1)
    MoveZeroNumInArray.move2(nums2)
    println("result -> ${nums2.toList()}")
}