package com.fsh.arithmeti.array;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * 题目：给你一个数组 nums 和一个值 val，你需要 原地 移除所有数值等于 val 的元素，并返回移除后数组的新长度。
 *
 * 不要使用额外的数组空间，你必须仅使用 O(1) 额外空间并 原地 修改输入数组。
 *
 * 元素的顺序可以改变。你不需要考虑数组中超出新长度后面的元素。
 *
 * 链接：https://leetcode-cn.com/problems/remove-element
 *
 *
 *
 */
public class RemoveElement {

    public static int removeElement(int[] nums,int val){
//        int arraySize = nums.length;
//        for(int index = nums.length-1;index>=0;index--){
//            if(nums[index] == value){
//                arraySize -= 1;
//                //不是最后一个数据
//                if(index != nums.length-1){
//                    movetoEnd(nums,index);
//                }
//            }
//        }
//
//        int[] result = new int[arraySize];
//        //int[] 在java 中不能实现原地删除元素，
//        System.arraycopy(nums,0,result,0,arraySize);
//
//        return arraySize;
        int i = 0;
        for (int j = 0; j < nums.length; j++) {
            if (nums[j] != val) {
                nums[i] = nums[j];
                i++;
            }
        }
        return i;
    }

//    private static void movetoEnd(int[] nums,int index){
//        int temp = nums[index];
//        for(int i = index;i < nums.length-1;i++){
//            nums[i] = nums[i+1];
//        }
//        nums[nums.length-1] = temp;
//    }

    public static void main(String[] args) {
        int[] array ={3,2,2,3};
        removeElement(array,3);
        for(int i : array){
            System.err.print(i+",");
        }
    }
}
