package com.fsh.arithmeti.array

/**
 * 将一个给定字符串根据给定的行数，以从上往下、从左到右进行 Z 字形排列。

比如输入字符串为 "LEETCODEISHIRING" 行数为 3 时，排列如下：

L   C   I   R
E T O E S I I G
E   D   H   N
之后，你的输出需要从左往右逐行读取，产生出一个新的字符串，比如："LCIRETOESIIGEDHN"。

请你实现这个将字符串进行指定行数变换的函数：

string convert(string s, int numRows);
示例 1:

输入: s = "LEETCODEISHIRING", numRows = 3
输出: "LCIRETOESIIGEDHN"
示例 2:

输入: s = "LEETCODEISHIRING", numRows = 4
输出: "LDREOEIIECIHNTSG"
解释:

L     D     R
E   O E   I I
E C   I H   N
T     S     G

链接：https://leetcode-cn.com/problems/zigzag-conversion
 */
object ZConvertString {
    /**
     * 解题思路：
     */
//    fun convert(s: String, numRows: Int):String{
//        if(numRows == 1) return s
//        val resultArray = arrayOfNulls<String>(numRows)
//        var index = 0
//        var flag = -1
//        for(value in s.toCharArray()){
//            if(resultArray[index] == null){
//                resultArray[index] = value.toString()
//            }else{
//                resultArray[index] = resultArray[index].plus(value)
//            }
//            if(index == 0 || index == numRows-1) flag = -flag
//            index+=flag
//        }
//        val sb = StringBuffer()
//        resultArray.forEach {
//            sb.append(it)
//        }
//        return sb.toString()
//    }
//
//    fun convert2(s:String,numRows:Int):String{
//        val resultArray = arrayOfNulls<String>(numRows)
//            val rowIndex = index % numRows
//            println("$index  in row $rowIndex")
//            if(resultArray[rowIndex] == null){
//                resultArray[rowIndex] = s.toCharArray()[index].toString()
//            }else{
//                resultArray[rowIndex] = resultArray[rowIndex].plus(s.toCharArray()[index])
//            }
//        }
//        val sb = StringBuffer()
//        resultArray.forEach {
//            sb.append(it)
//        }
//        return sb.toString()
//    }
}

fun main(args:Array<String>) {
//    val string = "LEETCODEISHIRING"
//    var numRows = 3
//    println(ZConvertString.convert(string,numRows))
//
//    println(ZConvertString.convert2(string,numRows))
}