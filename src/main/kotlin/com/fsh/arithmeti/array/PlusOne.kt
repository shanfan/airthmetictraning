package com.fsh.arithmeti.array

/**
 * 给定一个由整数组成的非空数组所表示的非负整数，在该数的基础上加一。

最高位数字存放在数组的首位， 数组中每个元素只存储单个数字。

你可以假设除了整数 0 之外，这个整数不会以零开头。

来源：力扣（LeetCode）
链接：https://leetcode-cn.com/problems/plus-one

示例 1:

输入: [1,2,3]
输出: [1,2,4]
解释: 输入数组表示数字 123。
示例 2:

输入: [4,3,2,1]
输出: [4,3,2,2]
解释: 输入数组表示数字 4321。
 */
object PlusOne {
    /**
     * 解题思路
     * https://leetcode-cn.com/problems/plus-one/solution/hua-jie-suan-fa-66-jia-yi-by-guanpengchn/
     */
    fun plus(array: IntArray):IntArray{
        for(index in array.size-1 downTo 0){
            array[index]++
            array[index] = array[index] % 10
            //这里纯在不为0 就没有进为的情况了，也不存在数据扩展，直接返回
            //妙
            if(array[index] != 0) return array
        }
        val result = IntArray(array.size+1)
        result[0] = 1
        return result
    }
}

fun main(args: Array<String>) {
    val nums = intArrayOf(9,8,6)
    val plus = PlusOne.plus(nums)
    println("plus result --> ${plus.toList()}")
}