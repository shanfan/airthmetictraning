package com.fsh.arithmeti.array

/**
 * 给定一个整数数组 nums 和一个目标值 target，请你在该数组中找出和为目标值的那 两个 整数，并返回他们的数组下标。

你可以假设每种输入只会对应一个答案。但是，数组中同一个元素不能使用两遍。

 

示例:

给定 nums = [2, 7, 11, 15], target = 9

因为 nums[0] + nums[1] = 2 + 7 = 9
所以返回 [0, 1]
 */
object TwoNumberSum {
    /**
     * 解题思路：利用hash表，快速查找数据，将数据和索引存储到hashMap中，
     * 数组元素对应的目标元素（target-nums[index]）是否存在hash表中，则我们找到了匹配的两个数字
     */
    fun twoNumSum(nums:IntArray,target:Int):IntArray{
        val map = HashMap<Int,Int>()
        nums.forEachIndexed {index,it->
            if(map.containsKey(target-it)){
                return intArrayOf(index, map[target-it]!!)
            }
            map[it] = index
        }
        return IntArray(0)
    }
}

fun main(args: Array<String>) {
    val nums = intArrayOf(2,7,11,12)
    val target = 9
    TwoNumberSum.twoNumSum(nums,target)
}

