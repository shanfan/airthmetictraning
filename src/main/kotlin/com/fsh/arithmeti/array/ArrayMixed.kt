package com.fsh.arithmeti.array

/**
 * 题目：给定两个数组，编写一个函数来计算它们的交集。
 * 示例 1:
 *     输入: nums1 = [1,2,2,1], nums2 = [2,2]
 *     输出: [2,2]
 * 示例 2:
 *     输入: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
 *     输出: [4,9]
 *
 * 分析：
 * @see {https://www.geekxh.com/1.0.%E6%95%B0%E7%BB%84%E7%B3%BB%E5%88%97/001.html#_02%E3%80%81%E9%A2%98%E8%A7%A3%E5%88%86%E6%9E%90}
 */
object ArrayMixed{
    /**
     * 先将数组1进行map映射，然后遍历数组2，在map中查找数组2中的值是否存在，
     * 如果存在直接保存到数组2中
     *
     * 这里使用了数组2来保存交集内容，避免了新开辟空间，节约了空间
     */
    fun method1(array1:IntArray,array2:IntArray):IntArray{
        val map = array1.associate { it to it }
        var k=0
        array2.forEach {
            if(map.containsKey(it)){
                array2[k] = it
                ++k
            }
        }
        return array2.sliceArray(IntRange(0,k-1))
    }

    /**
     * 如果是排序好的数组，声明两个数组的索引，对比两个索引位置的值是否相等
     *  相等，直接保存下来
     *  索引1 > 索引2 索引2往后移动
     *  索引1 < 索引2 索引1往后移动
     *
     *  这里使用了array1来保存交集内容，避免了新开辟空间，节约了空间
     */
    fun method2(array1: IntArray,array2: IntArray):IntArray{
        array1.sort()
        array2.sort()
        var index1 =0
        var index2 = 0
        var k =0
        while (index1 in array1.indices && index2 in array2.indices){
            when{
                array1[index1] > array2[index2] -> index2++
                array1[index1] < array2[index2] -> index1++
                else ->{
                    array1[k] = array1[index1]
                    k++
                    index1++
                    index2++
                }
            }
        }
        return array1.sliceArray(IntRange(0,k-1))
    }
}

fun main(args:Array<String>){
    val array1 = intArrayOf(1,2,3,5,7)
    val array2 = intArrayOf(1,3,3,5,9,10)
    val result1 = ArrayMixed.method1(array1,array2)
    val result2 = ArrayMixed.method2(array1,array2)
    println(result1.toList())
    println(result2.toList())
}