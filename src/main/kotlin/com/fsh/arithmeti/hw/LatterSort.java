package com.fsh.arithmeti.hw;

import java.util.*;

class Latter{
    char c;
    int count;
    Latter(char c,int cn){
        this.c = c;
        this.count = cn;
    }
}
public class LatterSort{

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){

            String input = sc.nextLine();
            char[] ca = input.toCharArray();
            Latter[] latter = new Latter[ca.length];
            ArrayList<Latter> list = new ArrayList();
            Map<Character,Integer> map = new HashMap();
            int index = 0;
            for(int i=0;i<ca.length;i++){
                char c = ca[i];
                if(c > 0 && c<128){
                    if(map.containsKey(c)){
                        Integer mi = map.get(c);
                        latter[mi].count += 1;
                    }else{
                        latter[index] = new Latter(c,1);
                        list.add(latter[index]);
                        map.put(c,index++);
                    }
                }
            }

            Collections.sort(list,new Comparator(){
                public int compare(Object ob1,Object ob2){
                    Latter l1 = (Latter)ob1;
                    Latter l2 = (Latter)ob2;
                    if(l1.count != l2.count){
                        return Integer.compare(l2.count,l1.count);
                    }
                    return Character.compare(l2.c,l1.c);
                }
            });
            //sort(latter,index);
            for(Latter l : list){
                if(l != null){
                    System.out.print(l.c);
                }

            }
            System.out.println();
        }
    }

    private static void sort(Latter[] ca,int index){
        for(int i=0;i<index;i++){
            for(int j=0;j<index-i-1;j++){
                if(ca[j].count < ca[i].count){
                    Latter tmp = ca[j];
                    ca[j] = ca[i];
                    ca[i] = tmp;
                }else if(ca[j].count == ca[i].count){
                    if(ca[j].c < ca[i].c){
                        Latter tmp = ca[j];
                        ca[j] = ca[i];
                        ca[i] = tmp;
                    }
                }
            }
        }
    }
}
