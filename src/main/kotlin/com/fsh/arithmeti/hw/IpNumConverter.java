package com.fsh.arithmeti.hw;

import java.util.Scanner;

public class IpNumConverter {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            String ip = sc.nextLine();
            String ipNum = sc.nextLine();
            System.out.println(ipToNum(ip));
            System.out.println(numToIp(ipNum));
        }
    }

    private static int ipToNum(String ip){
        if(!isIP(ip)){
            return 0;
        }
        String[] ipA = ip.split("\\.");
        StringBuilder sb = new StringBuilder();
        for(String s : ipA){
            int i = Integer.parseInt(s);
            String bs = Integer.toBinaryString(i);
            while(bs.length()<8){
                bs = "0"+bs;
            }
            sb.append(bs);
        }
        return Integer.parseInt(sb.toString(),2);
    }

    private static String numToIp(String ipNum){
        int ipN = Integer.parseInt(ipNum);
        String ipS = Integer.toBinaryString(ipN);
        while(ipS.length() < 32){
            ipS = "0"+ipS;
        }
        StringBuilder sb = new StringBuilder();
        while(ipS.length()>0){
            String ipB = ipS.substring(0,8);
            int i = Integer.parseInt(ipB,2);

            ipS = ipS.substring(8);
            sb.append(i).append(".");
        }
        return sb.toString();
    }

    private static boolean isIP(String ip){
        String[] ipA = ip.split("\\.");
        for(String num : ipA){
            int i = Integer.parseInt(num);
            if(i < 0 || i> 255){
                return false;
            }
        }
        return true;
    }
}
