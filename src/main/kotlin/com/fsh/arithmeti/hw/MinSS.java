package com.fsh.arithmeti.hw;

import java.util.Arrays;
import java.util.Scanner;

public class MinSS {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            int num = sc.nextInt();
            int minV = Integer.MAX_VALUE;
            int[] minA = new int[2];
            Arrays.fill(minA,0);
            for(int i=1;i<num;i++){
                int target = num - i;
                if(isN(target)){
                    if(Math.abs(target-i) < minV){
                        minV = Math.abs(target-i);
                        minA[0] = i;
                        minA[1] = target;
                    }
                }
            }
            System.out.println(minA[0]);
            System.out.println(minA[1]);
        }
    }

    private static boolean isN(int n){
        if(n == 1){
            return true;
        }
        for(int i=2;i<n;i++){
            if(n % i == 0 && i != 1){
                return false;
            }
        }

        return true;
    }
}
