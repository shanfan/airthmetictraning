package com.fsh.arithmeti.hw;

import java.util.*;
public class MinSanjiao {


    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            int N = Integer.parseInt(sc.nextLine());
            int[][] sanjiao = new int[N][N];
            for(int i=0;i<N;i++){
                String[] nums = sc.nextLine().split(" ");
                for(int j=0;j<nums.length;j++){
                    sanjiao[i][j] = Integer.parseInt(nums[j]);
                }
            }
            //这里是巨优化版本
            int[] R = new int[N];
            for(int i=0;i<N;i++){
                R[i] = sanjiao[N-1][i];
            }
            for(int i=N-2;i>=0;i--){
                for(int j=0;j<=i;j++){
                    R[j] = sanjiao[i][j] + Math.min(R[j],R[j+1]);
                }
            }
            System.out.println(R[0]);
            //初级版本
            int[][] dp = new int[N][N];
            dp[0][0] = sanjiao[0][0];
            dp[1][0] =  dp[0][0] + sanjiao[1][0];
            dp[1][1] =  dp[0][0] + sanjiao[1][1];
            int min = Integer.MAX_VALUE;
            for(int i=2;i<N;i++){
                for(int j=0;j<i+1;j++){
                    if(j==0){
                        dp[i][j] = dp[i-1][0] + sanjiao[i][j];
                    }else if(j == i){
                        dp[i][j] = dp[i-1][i-1] + sanjiao[i][j];
                    }else{
                        dp[i][j] = sanjiao[i][j] + Math.min(dp[i-1][j],dp[i-1][j-1]);
                    }

                }
            }
            for(int i=0;i<N;i++){
                min = Math.min(dp[N-1][i],min);
            }
            System.out.println(min);
        }
    }

}
