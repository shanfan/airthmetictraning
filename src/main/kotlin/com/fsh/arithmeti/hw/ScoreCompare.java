package com.fsh.arithmeti.hw;
import java.util.*;
class User{
    String name;
    int score;
    User(String n,int s){
        name = n;
        score = s;
    }
}
public class ScoreCompare {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            List<Integer> list = new ArrayList();
            String s1 = sc.nextLine();
            String s2 = sc.nextLine();
            String s3 = sc.nextLine();
            String s4 = sc.nextLine();
            add(list,s1);
            add(list,s2);
            add(list,s3);
            add(list,s4);
            Collections.sort(list);
            for(Integer i : list){
                System.out.print(i);
            }
            System.out.println();
        }
    }

    private static void add(List<Integer> list,String s){
        char[] ca = s.toCharArray();
        for(int i=0;i<ca.length;i++){
            int ii = ca[i]-'0';
            if(list.indexOf(ii) < 0){
                list.add(ii);
            }
        }
    }
}
