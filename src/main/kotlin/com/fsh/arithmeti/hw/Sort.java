package com.fsh.arithmeti.hw;

import java.util.Scanner;

public class Sort {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            String input = sc.nextLine();
            char[] ca = input.toCharArray();
            //Arrays.sort(ca);
            sort(ca);
            //input = new String(ca);
            System.out.println(ca);
        }
    }

    private static void sort(char[] arr){
        for(int i =0 ; i<arr.length-1 ; i++) {

            for(int j=0 ; j<arr.length-1-i ; j++) {

                if(arr[j]>arr[j+1]) {
                    char temp = arr[j];

                    arr[j]=arr[j+1];

                    arr[j+1]=temp;
                }
            }
        }
    }
}
