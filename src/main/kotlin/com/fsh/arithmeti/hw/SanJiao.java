package com.fsh.arithmeti.hw;

import java.util.*;
import java.util.List;
/**
 * 三角形到底边最大路径和
 *  测试数据: 先输入三角形有多少行，然后再输入三角形每行的数据
 *  输入：
 *  4
 *  2
 *  3 4
 *  6 5 7
 *  4 1 8 3
 *
 *  输出：
 *  21
 */
public class SanJiao {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            int rowCount = Integer.parseInt(sc.nextLine());
            int[][] sanjiao = new int[rowCount][rowCount];
            for(int i=0;i<rowCount;i++){
                String[] nums = sc.nextLine().split(" ");
                for(int j=0;j<nums.length;j++){
                    sanjiao[i][j] = Integer.parseInt(nums[j]);
                }
            }
            //递归计算
            System.out.println(getSanjiao1(sanjiao,0,0));
            //记忆递归型计算
            int[][] result1 = new int[rowCount][rowCount];
            for(int i=0;i<rowCount;i++){
                //-1表示未计算过
                Arrays.fill(result1[i],-1);
            }
            System.out.println(getSanjiao2(sanjiao,result1,0,0));
            System.out.println(getSanjiao3(sanjiao));
            System.out.println(getSanjiao4(sanjiao));
            System.out.println(getSanjiao5(sanjiao));
        }
    }

    /**
     * 方案1通过 递归，自顶向下，
     * 三角形数据存放在二维数组nums[][]中,三角形有N行数据，从第i行j列往底边走的时候，
     * 可以走[i+1,j]或者[i+1,j+1](正下方或者右下方的数字)，可以得到如下递归方程式
     *
     *
     * if(i == N)
     *     nums[i,j]             //最后一行的时候，数据就为num[i,j]
     * else
     *     max(num[i+1,j],num[i+1,j+1])+nums[i,j]   //只能往正下方或者右下角走，取两者中的最大值
     *
     *
     * @param nums  三角形数据
     * @param i     行号
     * @param j     列号
     * @return
     */
    private static int getSanjiao1(int[][] nums,int i,int j){
        if(i == nums.length-1){
            return nums[i][j];
        }
        return nums[i][j] + Math.max(getSanjiao1(nums,i+1,j),getSanjiao1(nums,i+1,j+1));
    }

    /**
     * 方案2 在 SanJiao#getSanjiao1 递归中，存在大量重复计算工作，耗时，时间复杂度为2^n
     * 这里采用一个二维数据，来记忆[i,j]的计算结果，如果已经计算过，就直接返回，不必再次计算
     * @param nums 三角形数据
     * @param result 计算结果记忆数组
     * @param i 行
     * @param j 列
     * @return 结果
     */
    private static int getSanjiao2(int[][] nums,int[][] result,int i,int j){
        //已经计算过，不再计算
        if(result[i][j] != -1){
            return result[i][j];
        }
        if(i == nums.length-1){
            result[i][j] = nums[i][j];
        }else{
            result[i][j] = nums[i][j] + Math.max(getSanjiao2(nums,result,i+1,j),
                    getSanjiao2(nums,result,i+1,j+1));
        }
        return result[i][j];
    }

    /**
     * 在前面两种方法中，都使用到了递归的方式，但是递归在程序运行时，是比较耗时和占内存的，会创建方法栈空间
     * 加深方法调用栈深度;
     * 这里采用循环的方式，通过递推的方式，自底向上推；
     * 最后一行的最大数据，就是[i,j],
     * 在 i行的时候，就由[i+1,j]和[i+1,j+1]这两个数字决定
     *
     *
     * @param nums
     * @return
     */
    private static int getSanjiao3(int[][] nums){
        int[][] result  = new int[nums.length][nums.length];
        //最后一行的数值就为num[i][j]
        for(int index=0;index<nums.length;index++){
            result[nums.length-1][index] = nums[nums.length-1][index];
        }
        for(int i=nums.length-2;i>=0;i--){
            for(int j=0;j<=i;j++){
                result[i][j] = nums[i][j] + Math.max(result[i+1][j],result[i+1][j+1]);
            }
        }
        return result[0][0];
    }

    /**
     * 这里 采用数据优化，下面一行的数据使用后，不会再使用，造成了内存浪费，直接 创建一维数组
     * @param nums
     * @return
     */
    private static int getSanjiao4(int[][] nums){
        int[] result= new int[nums.length];
        for(int i=0;i<nums.length;i++){
            result[i] = nums[nums.length-1][i];
        }
        for(int i=nums.length-2;i>=0;i--){
            for(int j=0;j<=i;j++){
                result[j] = nums[i][j] + Math.max(result[j],result[j+1]);
            }
        }
        return result[0];
    }

    /**
     * 在方案4的基础上，进一步优化，避免创建一维数组空间
     * @param nums
     * @return
     */
    private static int getSanjiao5(int[][] nums){
        //直接使用最后一行的引用，避免开辟新的数组空间
        int[] result = nums[nums.length-1];
        for(int i=nums.length-2;i>=0;i--){
            for(int j=0;j<=i;j++){
                result[j] = nums[i][j] + Math.max(result[j],result[j+1]);
            }
        }
        return result[0];
    }
}
