package com.fsh.arithmeti.hw;

import java.util.Arrays;
import java.util.Scanner;

public class PutApple {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            int appleCount = sc.nextInt();
            int bucketCount = sc.nextInt();
            System.out.println(pubApple(appleCount,bucketCount));
        }
    }

    private static int pubApple(int appleCount,int bucketCount){
        if(appleCount == 0){
            return 1;
        }
        if(bucketCount == 0){
            return 0;
        }
        if(bucketCount == 1){
            return 1;
        }
        if(appleCount < bucketCount){
            return pubApple(appleCount,appleCount);
        }
        return pubApple(appleCount-bucketCount,bucketCount) + pubApple(appleCount,bucketCount-1);
    }

}
