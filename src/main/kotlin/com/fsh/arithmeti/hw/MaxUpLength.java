package com.fsh.arithmeti.hw;
import java.util.*;
public class MaxUpLength {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            String input = sc.nextLine();
            System.out.println(maxLength(input));
        }
    }

    private static int maxLength(String input){
        int[] dp = new int[input.length()];
        Arrays.fill(dp,1);
        for(int i=1;i<input.length();i++){
            for(int j=0;j<i;j++){
                if(input.charAt(j) < input.charAt(i)){
                    dp[i] = Math.max(dp[j]+1,dp[i]);
                }
            }
        }
        Arrays.sort(dp);
        return dp[dp.length-1];
    }
}
