package com.fsh.arithmeti.hw;

import java.util.Scanner;

public class MaxCommonStrLength {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String s1 = sc.nextLine();
            String s2 = sc.nextLine();
            int[][] maxLen = new int[s1.length()+1][s2.length()+1];
            for(int i=0;i<s1.length();i++){
                maxLen[i][0] = 0;
            }
            for(int i=0;i<s2.length();i++){
                maxLen[0][i] = 0;
            }
            for(int i=1;i<=s1.length();i++){
                for(int j=1;j<=s2.length();j++){
                    if(s1.charAt(i-1) == s2.charAt(j-1)){
                        maxLen[i][j] = maxLen[i-1][j-1]+1;
                    }else{
                        maxLen[i][j] = Math.max(maxLen[i][j-1],maxLen[i-1][j]);
                    }
                }
            }
            System.out.println(maxLen[s1.length()][s2.length()]);
        }
    }

}
