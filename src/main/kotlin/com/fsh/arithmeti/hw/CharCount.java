package com.fsh.arithmeti.hw;

import java.util.*;

public class CharCount {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            //I数组
            String[] IArray = sc.nextLine().split(" ");
            //R数组
            String[] RArray = sc.nextLine().split(" ");
            Arrays.sort(RArray);
            TreeMap<Integer,List<String>> map = new TreeMap();
            StringBuilder sb = new StringBuilder();

            for(int i=1;i<RArray.length;i++){
                String Ri = RArray[i];
                List<String> list = findContainSNum(Ri,IArray);
                if(!list.isEmpty()){
                    map.put(Integer.parseInt(Ri),list);
                    sb.append(Ri);
                    sb.append(",");
                    sb.append(list.size());
                    sb.append(",");
                    for(String Ij : list){
                        sb.append(arrayIndex(IArray,Ij,1));
                        sb.append(",");
                        sb.append(Ij);
                    }
                }
            }
            int count = sb.toString().split(",").length;
            System.out.println(count+","+sb.toString());
        }
    }

    private static List<String> findContainSNum(String num,String[] IArray){
        List<String> list = new ArrayList();
        for(int i=0;i<IArray.length;i++){
            if(IArray[i].contains(num)){
                list.add(IArray[i]);
            }
        }
        return list;
    }

    private static int arrayIndex(String[] array,String value,int begin){
        for(int i=begin;i<array.length;i++){
            if(value == array[i]){
                return i;
            }
        }
        return -1;
    }


}
