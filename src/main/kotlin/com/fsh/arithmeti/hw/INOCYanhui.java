package com.fsh.arithmeti.hw;

import java.util.*;

public class INOCYanhui {
    public static void main(String args[]){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            int n = sc.nextInt();
            System.out.println(getOldIndex(n));
        }
    }

    private static int getOldIndex(int n){
        int[][] m = new int[n+1][2*n+1];
        m[1][0] = 1;
        for(int i=2;i<=n;i++){
            for(int j=0;j<=2*n;j++){
                m[i][j] = 0;
                int begin = j-3;
                int end = j;
                if(begin < 0){
                    begin = 0;
                }
                for(int ji = begin;ji<=end;ji++){
                    m[i][j] += m[i-1][ji];
                }
                if(m[i][j] %2 ==0){
                    return j+1;
                }

            }
        }

        return -1;
    }
}
