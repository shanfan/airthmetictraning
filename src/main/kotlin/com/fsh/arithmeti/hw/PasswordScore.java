package com.fsh.arithmeti.hw;
import java.util.*;
public class PasswordScore {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            String input = sc.nextLine();
            int lowLatter =0;
            int upLatter = 0;
            int numCount =0;
            int fuhaoCount =0;
            int score = 0;
            if(input.length() >=8){
                score += 25;
            }else if(input.length() >=5){
                score+=10;
            }else{
                score += 5;
            }
            for(int i=0;i<input.length();i++){
                char c = input.charAt(i);
                if(c >= '0' && c<='9'){
                    numCount += 1;
                }else if(c >='a' && c<='z'){
                    lowLatter +=1;
                }else if(c >='A' && c<='Z'){
                    upLatter +=1;
                }else{
                    fuhaoCount +=1;
                }
            }

            if(numCount == 1){
                score += 10;
            }else if(numCount > 1){
                score += 20;
            }
            //两种都有
            if(lowLatter> 0 && upLatter > 0){
                score += 20;
            }else if(lowLatter == input.length() || upLatter == input.length()){
                score += 10;
            }
            if(fuhaoCount == 1){
                score += 10;
            }else if(fuhaoCount > 1){
                score += 25;
            }
            //奖励
            if((upLatter > 0 && numCount >0) ||(lowLatter > 0 && numCount>0)){
                score +=2;
                if(fuhaoCount > 0){
                    score += 1;
                    if(upLatter > 0 && lowLatter>0 && numCount>0){
                        score +=2;
                    }
                }
            }
            /*if(upLatter > 0 && lowLatter > 0 && numCount> 0 && fuhaoCount>0){
                score +=5;
            }else if((upLatter > 0 || lowLatter > 0) && numCount> 0 && fuhaoCount>0){
                score +=3;
            }else if((upLatter > 0 || lowLatter > 0) && numCount> 0){
                score +=2;
            }*/

            if(score >= 90){
                System.out.println("VERY_SECURE");
            }else if(score >=80){
                System.out.println("SECURE");
            }else if(score>=70){
                System.out.println("VERY_STRONG");
            }else if(score>=60){
                System.out.println("STRONG");
            }else if(score>=50){
                System.out.println("AVERAGE");
            }else if(score>=25){
                System.out.println("WEAK");
            }else{
                System.out.println("VERY_WEAK");
            }

        }
    }
}
