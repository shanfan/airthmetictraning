package com.fsh.arithmeti.hw;

import java.util.*;

public class StringCombined {
//    public static void main(String[] args){
//        Scanner sc = new Scanner(System.in);
//        while(sc.hasNext()){
//            String line = sc.nextLine().replaceAll(" ","");
//            char[] result = line.toCharArray();
//            List<Character> l1 = new ArrayList();
//            List<Character> l2 = new ArrayList();
//            for(int i=0;i<result.length;i++){
//                if(i %2 == 0){
//                    l2.add(result[i]);
//                }else{
//                    l1.add(result[i]);
//                }
//            }
//            Collections.sort(l1);
//            Collections.sort(l2);
//            int i1Index = 0;
//            int i2Index = 0;
//            StringBuilder sb = new StringBuilder();
//            for(int i=0;i<result.length;i++){
//                if(i %2 == 0){
//                    result[i] = l2.get(i2Index++);
//                }else{
//                    result[i] = l1.get(i1Index++);
//                }
//                if(isIn(result[i])){
//                    //转为16进制int
//                    int tmpi = Integer.parseInt(""+result[i],16);
//                    StringBuilder tmpSb = new StringBuilder(Integer.toHexString(tmpi));
//                    String tmpS = tmpSb.reverse().toString();
//                    int r16Int = Integer.parseInt(tmpS,16);
//                    String s = Integer.toHexString(r16Int);
//                    System.out.println(result[i]+" ---> "+s);
//                    sb.append(s);
//                }else{
//                    sb.append(result[i]);
//                }
//            }
//            System.out.println(sb.toString());
//        }
//    }
//
//    private static boolean isIn(char c){
//        return (c>='0' && c<='9') || (c>='a' && c<='f') || (c>='A'&&c<='F');
//    }

    //第二次练习
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            String s1 = sc.next();
            String s2 = sc.next();
            mergeString(s1,s2);
        }

    }

    private static void mergeString(String s1,String s2){
        String merge = s1+s2;
        List<Character> odd = new ArrayList<>();//奇
        List<Character> even = new ArrayList<>();//偶
        for(int i=0;i<merge.length();i++){
            if(i %2 == 0){
                even.add(merge.charAt(i));
            }else{
                odd.add(merge.charAt(i));
            }
        }
        Collections.sort(odd);
        Collections.sort(even);
        char[] array = new char[merge.length()];
        for(int i=0;i<merge.length();i++){
            int realIndex = i/2;
            char c;
            if(i % 2 == 0){
                c = even.get(realIndex);
            }else{
                c = odd.get(realIndex);
            }
            if((c >='0' && c<='9') || (c>='A' && c<='F') || (c>='a' && c<='f')){
                int i1 = Integer.parseInt(c + "", 16);
                String s = Integer.toBinaryString(i1);
                StringBuilder sb = new StringBuilder("0000").append(s);
                s = sb.reverse().toString().substring(s.length()-4,s.length());
                int i2 = Integer.parseInt(s,2);
                s = Integer.toHexString(i2).toUpperCase();
                System.out.print(s);
            }
//            array[i] = c;
        }
//        System.out.println(array);
    }
}
