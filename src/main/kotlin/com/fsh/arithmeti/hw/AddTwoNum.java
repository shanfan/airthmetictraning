package com.fsh.arithmeti.hw;

import java.math.BigInteger;
import java.util.LinkedHashSet;
import java.util.Scanner;

public class AddTwoNum {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            String s1 = sc.nextLine();
            String s2 = sc.nextLine();
            BigInteger b1 = new BigInteger(s1);
            BigInteger b2 = new BigInteger(s2);
            String s = b1.add(b2).toString();
            System.out.println(s);
        }
    }
}
