package com.fsh.arithmeti.hw;

import java.util.*;

public class TargetNum {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            //pass
            String input = sc.nextLine();
            //unpass
            String passInput = sc.nextLine();
            char[] passOrigin = input.toCharArray();
            char[] passIOrigin = passInput.toCharArray();

            char[] passwords = new char[input.length()];
            encrpty(passOrigin,passwords);
            System.out.println(new String(passwords));

            char[] unPasswords = new char[input.length()];
            unEncrypt(unPasswords,passIOrigin);
            System.out.println(new String(unPasswords));
        }
    }
    //加密
    private static void encrpty(char[] aucPassword,char[] aucResult){
        for(int i=0;i<aucPassword.length;i++){
            char c = aucPassword[i];
            if(isNum(c)){
                if(c == 57){
                    aucResult[i] = '0';
                }else{
                    aucResult[i] = (char)(c+1);
                }
            }else if(isUpLatter(c)){
                if(c == 'Z'){
                    aucResult[i] = 'a';
                }else{
                    aucResult[i] = (char)(c + 33);
                }
            }else if(isLowLatter(c)){
                if(c == 'z'){
                    aucResult[i] = 'A';
                }else{
                    aucResult[i] = (char)(c - 31);
                }
            }else{
                aucResult[i] = c;
            }
        }
    }
    //解密
    private static void unEncrypt(char[] result,char[] password){
        for(int i=0;i<password.length;i++){
            char c = password[i];
            if(isNum(c)){
                if(c == '0'){
                    result[i] = '9';
                }else{
                    result[i] = (char)(c-1);
                }
            }else if(isUpLatter(c)){
                if(c=='A'){
                    result[i] = 'z';
                }else{
                    result[i] = (char)(c + 31);
                }
            }else if(isLowLatter(c)){
                if(c == 'a'){
                    result[i] = 'Z';
                }else{
                    result[i] = (char)(c - 33);
                }
            }else{
                result[i] = c;
            }
        }
    }

    private static boolean isNum(char c){
        return c >= '0' && c<='9';
    }

    private static boolean isUpLatter(char c){
        return (c >= 'A' && c<='Z');
    }
    private static boolean isLowLatter(char c){
        return (c >= 'a' && c<='z');
    }
}
