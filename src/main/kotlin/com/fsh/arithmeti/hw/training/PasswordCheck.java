package com.fsh.arithmeti.hw.training;

import java.util.Scanner;

public class PasswordCheck {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            String s = sc.nextLine();
            checkPassword(s);
        }
    }

    private static void checkPassword(String password){
        if(checkLength(password) && checkCharType(password) && checkRepeat(password)){
            System.out.println("OK");
        }else{
            System.out.println("NG");
        }
    }

    private static boolean checkLength(String password){
        return (password != null && password.length() >8);
    }

    private static boolean checkCharType(String password){
        boolean upLatter = false;
        boolean lowLatter = false;
        boolean unm = false;
        boolean other = false;
        for(int i=0;i<password.length();i++){
            char c = password.charAt(i);
            if(c >= '0' && c<='9'){
                unm = true;
            }else if(c >= 'A' && c<='Z'){
                upLatter = true;
            }else if(c>='a' && c<='z'){
                lowLatter = true;
            }else{
                other = true;
            }
            if(upLatter && lowLatter && unm && other){
                return  true;
            }
        }
        return false;
    }

    private static boolean checkRepeat(String password){
        for(int i=0;i<password.length()-2;i++){
            String sbS = password.substring(i,i+3);
            if(password.substring(i+1).contains(sbS)){
                return  false;
            }
        }
        return true;
    }

}
