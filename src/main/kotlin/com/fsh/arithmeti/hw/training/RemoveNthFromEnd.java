package com.fsh.arithmeti.hw.training;

class ListNode{
    int v;
    ListNode next;

    public ListNode(int v) {
        this.v = v;
    }
}
public class RemoveNthFromEnd {
    private static ListNode head = new ListNode(1);
    static {
        ListNode l5 = new ListNode(5);
        ListNode l4 = new ListNode(4);
        ListNode l3 = new ListNode(3);
        ListNode l2 = new ListNode(2);
        l2.next = l3;
        l3.next = l4;
        l4.next = l5;
//        head.next = l2;
    }

    public ListNode removeNthFromEnd(ListNode head,int n){
        ListNode dumpHead = new ListNode(Integer.MIN_VALUE);
        dumpHead.next = head;
        ListNode fast = dumpHead;
        ListNode slow = dumpHead;
        for(int i=0;i<=n;i++){
            fast = fast.next;
        }
        while (fast != null){
            fast = fast.next;
            slow = slow.next;
        }
        slow.next = slow.next.next;
        return dumpHead.next;
    }

    public static void main(String[] args) {
        ListNode result = new RemoveNthFromEnd().removeNthFromEnd(head, 1);
        while (result != null){
            System.out.print(result.v+" ---> ");
            result = result.next;
        }
    }
}
