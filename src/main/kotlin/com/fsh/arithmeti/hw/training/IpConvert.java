package com.fsh.arithmeti.hw.training;

import java.util.Scanner;

public class IpConvert {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            String ip = sc.nextLine();
            String ipNumS = sc.nextLine();
            System.out.println(ipToNum(ip));
            System.out.println(numToIP(ipNumS));
        }
    }

    private static long ipToNum(String ip){
        String[] split = ip.split("\\.");
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<split.length;i++){
            String tmp = split[i];
            int i1 = Integer.parseInt(tmp);
            if(i1 > 255){
                return -1;
            }
            tmp = Integer.toBinaryString(i1);
            while (tmp.length() < 8){
                tmp = "0" + tmp;
            }
            sb.append(tmp);
        }
        return Long.parseLong(sb.toString(),2);
    }

    private static String numToIP(String ipNum){
        long l = Long.parseLong(ipNum);
        String s = Long.toBinaryString(l);
        while (s.length() < 32){
            s = "0" +s;
        }
        StringBuilder sb = new StringBuilder();
        while (s.length() >=8){
            String tmp = s.substring(0,8);
            int i = Integer.parseInt(tmp, 2);
            if(i >255){
                return "";
            }
            sb.append(i).append(".");
            s = s.substring(8);
        }
        sb.delete(sb.length()-1,sb.length());
        return sb.toString();
    }
}
