package com.fsh.arithmeti.hw.training;

import java.util.Stack;
import java.util.TreeSet;

/**
 * 翻转部分链表
 */
public class ReverseListNodeBetween {



    static class ListNode{
        int val;
        ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }

    static ListNode reverseBetween(ListNode head,int m,int n){
        ListNode dump = new ListNode(Integer.MIN_VALUE);
        dump.next = head;
        ListNode p = dump;
        ListNode p1 = head;
        for(int i=1;i<m;i++){
            p = p1;
            p1 = p1.next;
        }
        for(int i=0;i<n-m;i++){
            ListNode tmp = p1.next;
            p1.next = tmp.next;
            tmp.next = p.next;
            p.next = tmp;
        }
        return dump.next;
    }

    static ListNode head = new ListNode(1);
    static {
        ListNode l2 = new ListNode(2);
        ListNode l3 = new ListNode(3);
        ListNode l4 = new ListNode(4);
        ListNode l5 = new ListNode(5);
        ListNode l6 = new ListNode(6);
        ListNode l7 = new ListNode(7);
        l2.next = l3;
        l3.next = l4;
        l4.next = l5;
        l5.next = l6;
        l6.next = l7;
        head.next = l2;
    }

    public static void main(String[] args) {
        ListNode tl1 = head;
        while (tl1 != null){
            if(tl1.next != null){
                System.out.print(tl1.val+" --> ");
            }else{
                System.out.println(tl1.val);
            }
            tl1 = tl1.next;
        }
        tl1 = reverseBetween(head,2,5);
        while (tl1 != null){
            if(tl1.next != null){
                System.out.print(tl1.val+" --> ");
            }else{
                System.out.println(tl1.val);
            }
            tl1 = tl1.next;
        }
    }

}
