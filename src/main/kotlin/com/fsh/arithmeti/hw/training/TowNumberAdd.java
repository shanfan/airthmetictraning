package com.fsh.arithmeti.hw.training;

import java.util.HashMap;
import java.util.Map;

public class TowNumberAdd {
    public static int[] twoSum(int[] numbers,int target){
        Map<Integer,Integer> map = new HashMap<>();
        for(int i=0;i<numbers.length;i++){
            int v = target - numbers[i];
            if(map.containsKey(v)){
                int[] r = new int[2];
                r[0] = map.get(v);
                r[1] = i+1;
                return r;
            }
            map.put(numbers[i],i+1);
        }
        return new int[0];
    }

    public static void main(String[] args) {
        int[] numbers = {3,2,4};
        int target = 6;
        int[] r = twoSum(numbers, target);
        System.out.println(r);
    }
}
