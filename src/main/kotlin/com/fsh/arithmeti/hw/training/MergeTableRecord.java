package com.fsh.arithmeti.hw.training;

import java.util.Arrays;
import java.util.Scanner;

/**
 * 题目描述
 * 数据表记录包含表索引和数值（int范围的整数），请对表索引相同的记录进行合并，即将相同索引的数值进行求和运算，输出按照key值升序进行输出。
 *
 * 输入描述:
 * 先输入键值对的个数
 * 然后输入成对的index和value值，以空格隔开
 *
 * 输出描述:
 * 输出合并后的键值对（多行
 */
public class MergeTableRecord {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            int count= Integer.parseInt(sc.nextLine());
            int[] array = new int[count];
            Arrays.fill(array,Integer.MIN_VALUE);
            for(int i=0;i<count;i++){
                String[] nums = sc.nextLine().split(" ");
                int index = Integer.parseInt(nums[0]);
                int value = Integer.parseInt(nums[1]);
                if(array[index] == Integer.MIN_VALUE){
                    array[index] = value;
                }else{
                    array[index] += value;
                }
            }
            for(int i=0;i<array.length;i++){
                if(array[i] != Integer.MIN_VALUE){
                    System.out.println(i+" "+array[i]);
                }
            }

        }
    }
}
