package com.fsh.arithmeti.hw.training;

import java.util.Scanner;

public class StringEncryptAndDecrypt {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            String input = sc.nextLine();
            String password = encrypt(input);
            System.out.println(password);
            System.out.println(decrypt(password));
        }
    }

    private static String encrypt(String input){
        char[] chars = input.toCharArray();
        for(int i=0;i<chars.length;i++){
            char c = chars[i];
            if(c >='A' && c<='Z'){
                c = (char) (c+32+1);
                if(c > 'z'){
                  c =  'a';
                }
            }else if(c >='a' && c<='z'){
                //小写变大写，然后移动一位
                c = (char) (c -32 +1);
                if(c > 'Z'){
                    c = 'A';
                }
            }else if(c >='0' && c<='9'){
                c = (char) (c+1);
                if(c > '9'){
                    c = '0';
                }
            }
            chars[i] = c;
        }
        return new String(chars);
    }

    private static String decrypt(String password){
        char[] chars = password.toCharArray();
        for(int i=0;i<chars.length;i++){
            char c = chars[i];
            if(c >='a' && c<='z'){
                //小写，先完后移动一位，并且变大写
                c = (char) (c -1 -32);
                if(c <'A'){
                    c = 'Z';
                }
            }else if(c>='A' && c<='Z'){
                //大写，先移动一位，然后变小写;
                c = (char) (c-1 +32);
                if(c  < 'a'){
                    c = 'z';
                }
            }else if(c >='0' && c<='9'){
                c = (char) (c-1);
                if(c <'0'){
                    c = '9';
                }
            }
            chars[i] = c;
        }
        return new String(chars);
    }
}
