package com.fsh.arithmeti.hw.training;

import java.util.Scanner;

/**
 * 题目描述
 * 将一个英文语句以单词为单位逆序排放。例如“I am a boy”，逆序排放后为“boy a am I”
 * 所有单词之间用一个空格隔开，语句中除了英文字母外，不再包含其他字符
 *
 *
 * 接口说明
 *
 * **
 *  * 反转句子
 *  *
 *  * @param sentence 原句子
 *  * @return 反转后的句子
 *public String reverse(String sentence);
        *
        *
        *
        *
        *
        *
        *
        *输入描述:
        *将一个英文语句以单词为单位逆序排放。
        *
        *输出描述:
        *得到逆序的句子
 */
public class SegmentReverse {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            String[] line = sc.nextLine().split(" ");
            int i =0,j= line.length-1;
            while (i<j){
                String tmp = line[i];
                line[i] = line[j];
                line[j] = tmp;
                i++;
                j--;
            }
            for(int in=0;in<line.length;in++){
                System.out.print(line[in]+" ");
            }
            System.out.println();
        }
    }
}
