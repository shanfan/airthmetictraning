package com.fsh.arithmeti.hw.training;

import java.util.*;

public class FamaWeight {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            int famaCount = sc.nextInt();
            int[] weight = new int[famaCount];
            int[] nums = new int[famaCount];
            for(int i=0;i<famaCount;i++){
                weight[i] = sc.nextInt();
            }
            for(int j=0;j<famaCount;j++){
                nums[j] = sc.nextInt();
            }
            Set<Integer> weightSet = new LinkedHashSet<>();
            weightSet.add(0);
            for(int i=0;i<famaCount;i++){
                List<Integer> tmp = new ArrayList<>(weightSet);
                for(int j=0;j<=nums[i];j++){
                    for(int k=0;k<tmp.size();k++){
                        weightSet.add(tmp.get(i) + weight[i]*j);
                    }
                }
            }
            System.out.println(weightSet.size());
        }
    }
}
