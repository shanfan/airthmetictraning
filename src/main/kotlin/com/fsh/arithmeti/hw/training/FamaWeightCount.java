package com.fsh.arithmeti.hw.training;

import java.util.*;

public class FamaWeightCount {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            int count = Integer.parseInt(sc.nextLine());
            String[] s1 = sc.nextLine().split(" ");
            String[] s2 = sc.nextLine().split(" ");
            int[] weight = new int[count];
            int[] nums = new int[count];
            for(int i=0;i<s1.length;i++){
                weight[i] = Integer.parseInt(s1[i]);
            }
            for(int i=0;i<s2.length;i++){
                nums[i] = Integer.parseInt(s2[i]);
            }
            List<Integer> set = new ArrayList<>();
            set.add(0);
            for(int i=0;i<count;i++){
                List<Integer> tmp = new ArrayList<>(set);
                for(int j=0;j<=nums[i];j++){
                    for(int k=0;k<tmp.size();k++){
                        set.add(tmp.get(k)+weight[i]*j);
                    }
                }
            }
            System.out.println(new HashSet<Integer>(set).size());
        }
    }
}
