package com.fsh.arithmeti.hw.training;

import java.util.Scanner;

/**
 * 题目描述
 * 写出一个程序，接受一个正浮点数值，输出该数值的近似整数值。如果小数点后数值大于等于5,向上取整；小于5，则向下取整。
 *
 * 输入描述:
 * 输入一个正浮点数值
 *
 * 输出描述:
 * 输出该数值的近似整数值
 */
public class CloselyNumber {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            double input = sc.nextDouble();
            double floor = Math.floor(input);
            if(input - floor >= 0.5){
                System.out.println((int)Math.round(input));
            }else{
                System.out.println((int)floor);
            }
        }
    }
}
