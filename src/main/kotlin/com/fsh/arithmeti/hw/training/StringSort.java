package com.fsh.arithmeti.hw.training;

import java.util.Scanner;

public class StringSort {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            String input = sc.nextLine();
            sortString(input);
        }
    }

    private static void sortString(String input){
        StringBuilder sb = new StringBuilder();
        for (int i=0;i<26;i++){
            char c = (char) (i+'A');
            for(int j=0;j<input.length();j++){
                char ic = input.charAt(j);
                if(ic == c ||(c+32)==ic){
                    sb.append(ic);
                }
            }
        }
        for(int i=0;i<input.length();i++){
            char c = input.charAt(i);
            if(!((c >='a' && c<='z') || (c>='A' && c<='Z'))){
                sb.insert(i,c);
            }
        }
        System.out.println(sb.toString());
    }
}
