package com.fsh.arithmeti.hw.training;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IsPalindrome {
    static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }
    static ListNode head = new ListNode(-129);
    static {
        ListNode l1 = new ListNode(-127);
        head.next = l1;
    }

    public static void main(String[] args) {
        System.out.println(isPalindrome(head));
        System.out.println(isPalindrome2(head));
    }

    public static boolean isPalindrome(ListNode head){
        ListNode dump = head;
        List<Integer> l1 = new ArrayList<>();
        while (dump != null){
            l1.add(dump.val);
            dump = dump.next;
        }
        List<Integer> l2 = new ArrayList<>(l1.size());
        for(int i=l1.size()-1;i>=0;i--){
            l2.add(l1.get(i));
        }
        Integer[] a1 = new Integer[l1.size()];
        Integer[] a2 = new Integer[l2.size()];
        l1.toArray(a1);
        l2.toArray(a2);
        return Arrays.equals(a1,a2);
    }

    public static boolean isPalindrome2(ListNode head){
        ListNode reverse = reverse(head);
        while (head != null && reverse != null){
            if(head.val != reverse.val){
                return false;
            }
            head = head.next;
            reverse = reverse.next;
        }
        return true;
    }


    private static ListNode reverse(ListNode head){
        ListNode p = head;
        ListNode pre = null;
        while (p != null){
            ListNode c = new ListNode(p.val);
            p = p.next;
            c.next = pre;
            pre = c;
        }
        return pre;
    }

}
