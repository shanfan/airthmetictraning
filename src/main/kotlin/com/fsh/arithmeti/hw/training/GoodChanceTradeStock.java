package com.fsh.arithmeti.hw.training;

public class GoodChanceTradeStock {
    /**
     * 只能买入一次卖出一次
     * @param prices
     * @return
     */
    public int maxProfit(int[] prices){
        int minValue = prices[0];
        int maxProfit = 0;
        for(int i=1;i<prices.length;i++){
            int tmp = prices[i] - minValue;
            if(tmp < 0){
                minValue = prices[i];
            }else{
                if(maxProfit < tmp){
                    maxProfit = tmp;

                }
            }
        }
        return maxProfit;
    }


    public int maxProfit2(int[] prices){
        int maxProfit = 0;
        for(int i=1;i<prices.length;i++){
            if(prices[i] > prices[i-1]){
                maxProfit += prices[i] - prices[i-1];
            }
        }
        return maxProfit;
    }

    public static void main(String[] args) {
        int[] data = {1,4,2,1,5,9,8};
        GoodChanceTradeStock goodChance = new GoodChanceTradeStock();
        System.out.println("只买卖一次:"+goodChance.maxProfit(data));
        System.out.println("多次买卖，但只有买之后才能卖:"+goodChance.maxProfit2(data));
    }
}
