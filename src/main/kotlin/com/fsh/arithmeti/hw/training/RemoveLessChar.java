package com.fsh.arithmeti.hw.training;

import java.util.*;

/**
 * 题目描述
 * 实现删除字符串中出现次数最少的字符，若多个字符出现次数一样，则都删除。输出删除这些单词后的字符串，字符串中其它字符保持原来的顺序。
 * 注意每个输入文件有多组输入，即多个字符串用回车隔开
 * 输入描述:
 * 字符串只包含小写英文字母, 不考虑非法输入，输入的字符串长度小于等于20个字节。
 *
 * 输出描述:
 * 删除字符串中出现次数最少的字符后的字符串。
 */
public class RemoveLessChar {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            String s = sc.nextLine();
            remove(s);
        }
    }

    private static void remove(String input){
        Map<Character,Integer> charCountMap = new HashMap<>();
        for(int i=0;i<input.length();i++){
            char c = input.charAt(i);
            if(charCountMap.containsKey(c)){
                charCountMap.put(c,charCountMap.get(c)+1);
            }else{
                charCountMap.put(c,1);
            }
        }
        int minCount = Integer.MAX_VALUE;
        Map<Integer, Set<Character>> map = new HashMap<>();
        for(Map.Entry<Character,Integer> e : charCountMap.entrySet()){
            minCount = Math.min(minCount,e.getValue());
            Set<Character> characters = map.get(e.getValue());
            if(characters == null){
                characters = new HashSet<>();
                map.put(e.getValue(),characters);
            }
            characters.add(e.getKey());
        }
        Set<Character> characters = map.get(minCount);
        for(Character c : characters){
            input = input.replaceAll(c.toString(),"");
        }
        System.out.println(input);
    }
}
