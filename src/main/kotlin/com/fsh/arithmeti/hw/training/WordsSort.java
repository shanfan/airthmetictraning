package com.fsh.arithmeti.hw.training;

import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

/**
 * 题目描述
 * 给定n个字符串，请对n个字符串按照字典序排列。
 * 输入描述:
 * 输入第一行为一个正整数n(1≤n≤1000),下面n行为n个字符串(字符串长度≤100),字符串中只含有大小写字母。
 * 输出描述:
 * 数据输出n行，输出结果为按照字典序排列的字符串。
 */
public class WordsSort {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            int count = Integer.parseInt(sc.nextLine());
            Set<String> set = new TreeSet<>();
            for(int i=0;i<count;i++){
                set.add(sc.nextLine());
            }
            for(String s:set){
                System.out.println(s);
            }
        }
    }
}
