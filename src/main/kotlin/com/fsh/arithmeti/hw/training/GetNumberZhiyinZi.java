package com.fsh.arithmeti.hw.training;

import java.util.*;

/**
 * 题目描述
 * 功能:输入一个正整数，按照从小到大的顺序输出它的所有质因子（重复的也要列举）（如180的质因子为2 2 3 3 5 ）
 * <p>
 * 最后一个数后面也要有空格
 * <p>
 * 输入描述:
 * 输入一个long型整数
 * <p>
 * 输出描述:
 * 按照从小到大的顺序输出它的所有质数的因子，以空格隔开。最后一个数后面也要有空格。
 */
public class GetNumberZhiyinZi {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            long num = sc.nextLong();
            List<Long> list = new ArrayList<>();
            for (long i = 2; i <= num; i++) {
                while (num % i == 0) {
                    list.add(i);
                    num = num / i;
                }
            }
            Collections.sort(list);
            for(Long l : list){
                System.out.print(l+" ");
            }
            System.out.println();
        }
    }
}
