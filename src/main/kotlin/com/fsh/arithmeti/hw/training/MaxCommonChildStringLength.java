package com.fsh.arithmeti.hw.training;

import java.util.Arrays;
import java.util.Scanner;
//abcfbc
//abfcab

/**
 * 公共最长子字符串
 */
public class MaxCommonChildStringLength {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            String s1 = sc.nextLine();
            String s2 = sc.nextLine();
            //初始化边界条件
            //maxLen(n,0) = 0
            //maxLen(0,n) = 0;
            int[][] dp = new int[s1.length()+1][s2.length()+1];

            for(int i=1;i<=s1.length();i++){
                for(int j=1;j<=s2.length();j++){
                    if(s1.charAt(i-1) == s2.charAt(j-1)){
                        dp[i][j] = dp[i-1][j-1]+1;
                    }else{
                        dp[i][j]= Math.max(dp[i-1][j],dp[i][j-1]);
                    }
                }
            }
            System.out.println(dp[s1.length()][s2.length()]);
        }
    }
}
