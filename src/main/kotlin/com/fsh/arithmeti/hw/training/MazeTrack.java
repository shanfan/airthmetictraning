package com.fsh.arithmeti.hw.training;

import java.util.Scanner;
import java.util.Stack;

/**
 * 走迷宫问题\
 * 输入描述:
 * 输入两个整数，分别表示二位数组的行数，列数。再输入相应的数组，其中的1表示墙壁，0表示可以走的路。数据保证有唯一解,不考虑有多解的情况，即迷宫只有一条通道。
 * <p>
 * 输出描述:
 * 左上角到右下角的最短路径，格式如样例所示。
 */
public class MazeTrack {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int rowCount = sc.nextInt();
            int colCount = sc.nextInt();
            int[][] maze = new int[rowCount][colCount];
            for (int i = 0; i < rowCount; i++) {
                for (int j = 0; j < colCount; j++) {
                    maze[i][j] = sc.nextInt();
                }
            }
            mazeTrack(maze, 0, 0, rowCount, colCount, new Stack<>());
        }
    }

    private static void mazeTrack(int[][] maze, int x, int y, int row, int col, Stack<Point> stack) {
        stack.push(new Point(x, y));
        maze[x][y] = 1;
        if (col == y + 1 && x + 1 == row) {
            for (Point p : stack) {
                System.out.println(String.format("(%d,%d)", p.x, p.y));
            }
        }
        if (x + 1 < row && maze[x + 1][y] == 0) {
            mazeTrack(maze, x + 1, y, row, col, stack);
        }
        if (y + 1 < col && maze[x][y + 1] == 0) {
            mazeTrack(maze, x, y + 1, row, col, stack);
        }
        maze[x][y] = 0;
        stack.pop();
    }

    static class Point {
        int x;
        int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

}


