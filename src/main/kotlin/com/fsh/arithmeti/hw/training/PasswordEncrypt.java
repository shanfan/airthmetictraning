package com.fsh.arithmeti.hw.training;

import java.util.Scanner;

public class PasswordEncrypt {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            String key = sc.nextLine();
            key = getKey(key);
            char[] cData  = sc.nextLine().toCharArray();
            for(int i=0;i<cData.length;i++){
                int index = getLatterIndex(cData[i]);
                if(index>=0){
                    if(cData[i] >='a' && cData[i]<='z'){
                        cData[i] = (char)(key.charAt(index) + 32);
                    }else{
                        cData[i] = key.charAt(index);
                    }
                }
            }
            System.out.println(cData);
        }
    }

    private static int getLatterIndex(char c){
        if(c >='a' && c<='z'){
            return c -'a';
        }else if(c >='A' && c <='Z'){
            return c -'A';
        }
        return -1;
    }

    private static String getKey(String key){
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<key.length();i++){
            String cs = (""+key.charAt(i)).toUpperCase();
            if(sb.indexOf(cs) <0){
                sb.append(cs);
            }
        }
        int beginIndex = sb.length();
        if(beginIndex == 26){
            return sb.toString();
        }
        for(int i=0;i<26;i++){
            char c = (char)(i+'A');
            if(sb.indexOf(c+"") < 0){
                sb.append(c);
            }
            if(sb.length() == 26){
                break;
            }
        }
        return sb.toString();
    }
}
