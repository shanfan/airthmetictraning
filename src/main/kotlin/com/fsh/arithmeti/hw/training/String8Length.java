package com.fsh.arithmeti.hw.training;

import java.util.Scanner;

/**
 * 题目描述
 * •连续输入字符串，请按长度为8拆分每个字符串后输出到新的字符串数组；
 * •长度不是8整数倍的字符串请在后面补数字0，空字符串不处理。
 * 输入描述:
 * 连续输入字符串(输入2次,每个字符串长度小于100)
 *
 * 输出描述:
 * 输出到长度为8的新字符串数组
 */
public class String8Length {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            String input = sc.nextLine();
            if(input.length()%8 != 0){
                input += "00000000";
            }
            while (input.length()>=8){
                System.out.println(input.substring(0,8));
                input = input.substring(8);
            }
        }
    }
}
