package com.fsh.arithmeti.hw.training;

import java.util.Scanner;

/**
 * 题目描述
 * 输入一个int型整数，按照从右向左的阅读顺序，返回一个不含重复数字的新的整数。
 *
 * 输入描述:
 * 输入一个int型整数
 *
 * 输出描述:
 * 按照从右向左的阅读顺序，返回一个不含重复数字的新的整数
 */
public class LRUniqueNumber {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            String input = sc.nextLine();
            StringBuilder sb = new StringBuilder();
            for(int i=input.length()-1;i>=0;i--){
                if(sb.indexOf(input.substring(i,i+1)) < 0 ){
                    sb.append(input.charAt(i));
                }
            }
            System.out.println(sb.toString());

        }
    }
}
