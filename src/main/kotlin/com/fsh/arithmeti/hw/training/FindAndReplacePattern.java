package com.fsh.arithmeti.hw.training;

import java.util.*;

public class FindAndReplacePattern {
     public static List<String> match(String[] words,String pattern){
         int[] patternArray = wordToIntArray(pattern);
         List<String> list = new ArrayList<>();
         for(int i=0;i<words.length;i++){
             String word = words[i];
             if(word.length() == pattern.length() && Arrays.equals(wordToIntArray(word),patternArray)){
                 list.add(word);
             }
         }
         return list;
     }

     static int[] wordToIntArray(String word){
         int[] array = new int[word.length()];
         for(int i=0;i<array.length;i++){
             array[i] = word.indexOf(word.charAt(i));
         }
         return array;
     }

    public static void main(String[] args) {
        String[] words = {"abc","deq","mee","aqq","dkd","ccc"};
        String pattern = "abb";
        System.out.println(match(words,pattern));
    }
}
