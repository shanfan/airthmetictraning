package com.fsh.arithmeti.hw.training;

import java.util.Scanner;

/**
 * 最大上升子序列
 * 1.确认问题
 * 如果以 求前n个子元素的最长上升子序列，F(n) = x,推导不出F(n+1)的关系，因为An+1不可能会比An大，不一定能够成最大上升子序列
 * 这里切换问题为，求以An+1为终点，能构成的最大上升子序列，那么F(n) = Max{maxLen(i): 1<=i<n,且ai< ak 且k!=1}+1,
 * 如果找不到，则最大长度为1
 */
public class MaxRiseStringLength {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            String input = sc.nextLine();
            int[] dp = new int[input.length()];
            dp[0] = 1;
            for(int i=1;i<input.length();i++){
                for(int j=0;j<=i;j++){
                    if(input.charAt(i) > input.charAt(j)){
                        dp[i] = Math.max(dp[j]+1,dp[i]);
                    }
                }
            }
            int max = Integer.MIN_VALUE;
            for(int v : dp){
                if(v > max){
                    max = v;
                }
            }
            System.out.println(max);
        }
    }
}
