package com.fsh.arithmeti.hw.training;

import java.util.*;

class MeasureResult{
    int id;
    int result;

    MeasureResult(int id, int result) {
        this.id = id;
        this.result = result;
    }
}
public class LinearInsert {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()){
            int dataCount = scanner.nextInt();
            scanner.nextInt();
            List<MeasureResult> list = new ArrayList<>();
            Map<Integer,Integer> map = new HashMap<>();
            for(int i=0;i<dataCount;i++){
                int id = scanner.nextInt();
                int result = scanner.nextInt();
                if(map.containsKey(id)){
                    continue;
                }
                map.put(id,id);

                if(list.isEmpty() || list.get(list.size()-1).id <id || list.get(list.size()-1).id - id == 1){
                    list.add(new MeasureResult(id,result));
                }else{
                    MeasureResult last = list.get(list.size()-1);
                    int lastResult = last.result;
                    int lastId = last.id;
                    //需要补充数据
                    for(int i1 = 1;i1 <= id - lastId-1;i1++){
                        int fillId = lastId+i1;

                        int fillResult = lastResult + (result - lastResult)/(id-lastId) * i1;
                        list.add(new MeasureResult(fillId,fillResult));
                    }
                    list.add(new MeasureResult(id,result));
                }
            }
            for(MeasureResult mr : list){
                System.out.println(mr.id +" "+mr.result);
            }
        }
    }
}
