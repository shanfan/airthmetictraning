package com.fsh.arithmeti.hw.training;

import java.util.Scanner;

public class NameBeauty {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            String input = sc.next();
            int len = sc.nextInt();
            System.out.println(split(input,len));
        }
    }

    private static String split(String input,int len){
        char[] ca = input.toCharArray();
        StringBuilder  sb = new StringBuilder();
        int index =0;
        int i=0;
        while (index < len){
            char c = ca[i++];

            if(c <= 127){
                index++;
                sb.append(c);
            }else{
                index += 2;
                if(index <=len){
                    sb.append(c);
                }
            }
        }
        return sb.toString();
    }
}
