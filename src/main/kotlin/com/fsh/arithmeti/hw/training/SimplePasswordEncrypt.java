package com.fsh.arithmeti.hw.training;

import java.util.Scanner;

/**
 * 简单密码加密
 * 大写字母，变小写，然后往后移一位，如果是z就变成了a
 */
public class SimplePasswordEncrypt {
    static final String PATTERN = "abcdefghijklmnopqrstuvwxyz";
    static final String PASSWORD = "22233344455566677778889999";

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            String input = sc.nextLine();
            encrypt(input);
        }
    }

    private static void encrypt(String input){
        char[] result = new char[input.length()];
        char[] origin = input.toCharArray();
        for(int i=0;i<origin.length;i++){
            //大写就变小写
            if(origin[i] >='A' && origin[i] <= 'Z'){
                char rc = (char)( origin[i]+1+32);
                if(rc >'z'){
                    rc = 'a';
                }
                result[i] = rc;
            }else if(origin[i] >='a' && origin[i] <='z'){
                int i1 = PATTERN.indexOf(origin[i] + "");
                result[i] = PASSWORD.charAt(i1);
            }else{
                result[i] = origin[i];
            }
        }
        System.out.println(result);
    }
}
