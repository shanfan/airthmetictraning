package com.fsh.arithmeti.hw.training;

import java.util.*;

public class FindBrotherWords {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            String s = sc.nextLine();
            String[] s1 = s.split(" ");
            String searchKey = s1[s1.length-2];
            char[] searchKeyArray = searchKey.toCharArray();
            Arrays.sort(searchKeyArray);
            int brokerIndex = Integer.parseInt(s1[s1.length-1]);
            List<String> brotherWords = new ArrayList<>();
            for(int i=1;i<s1.length-2;i++){
                String word = s1[i];
                if(word.equals(searchKey)){
                    continue;
                }
                char[] chars = word.toCharArray();
                Arrays.sort(chars);
                if(Arrays.equals(chars,searchKeyArray)){
                    brotherWords.add(word);
                }
            }
            Collections.sort(brotherWords);
            System.out.println(brotherWords.size());
            if(brotherWords.size() >= brokerIndex){
                System.out.println(brotherWords.get(brokerIndex-1));
            }
        }
    }
}
