package com.fsh.arithmeti.hw.training;

import java.util.Scanner;

public class LastWordsLength {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            String input = sc.nextLine().trim();
            String[] words = input.split(" ");
            System.out.println(words[words.length-1].length());
        }
    }
}
