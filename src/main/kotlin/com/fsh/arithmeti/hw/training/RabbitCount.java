package com.fsh.arithmeti.hw.training;

import java.util.Scanner;

public class RabbitCount {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            int month = sc.nextInt();
            System.out.println(getCount(month));
        }
    }

    private static int getCount(int month){
        if(month < 3){
            return 1;
        }
        return getCount(month-1) + getCount(month-2);
    }
}
