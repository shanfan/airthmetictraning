package com.fsh.arithmeti.hw.training;

import java.util.Scanner;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ThreadPrint {
     static AtomicInteger countA = new AtomicInteger(0);
    static AtomicInteger STATUS = new AtomicInteger(0);
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            int i = sc.nextInt();
            ThreadGroup tg = new ThreadGroup("Print");
            ReentrantLock lock = new ReentrantLock();
            Condition A = lock.newCondition();
            Condition B = lock.newCondition();
            Condition C = lock.newCondition();
            Condition D = lock.newCondition();
            Thread TA = crateThread(tg,lock,A,B,i,'A');
            Thread TB = crateThread(tg,lock,B,C,i,'B');
            Thread TC = crateThread(tg,lock,C,D,i,'C');
            Thread TD = crateThread(tg,lock,D,A,i,'D');
            TA.start();
            safeSleep(100);
            TB.start();
            safeSleep(100);
            TC.start();
            safeSleep(100);
            TD.start();
            while (tg.activeCount() > 0){
                Thread.yield();
            }
        }
    }

    private static void safeSleep(long time){
        try{
            Thread.sleep(time);
        }catch (Exception e){

        }
    }

    private static Thread crateThread(ThreadGroup tg,ReentrantLock lock,Condition condition,Condition nextCondition,int printCount,char printChar){
        return new Thread(new Printer(printCount,lock,condition,nextCondition,printChar,printChar=='D'));
    }

    static class Printer implements Runnable{
        private final int printCount;
        private final ReentrantLock lock;
        private final Condition thisCondition;
        private final Condition nextCondition;
        private final char printChar;
        private final boolean isLastChar;
        public Printer(int printCount, ReentrantLock lock, Condition thisCondition, Condition nextCondition, char printChar,boolean isLastChar) {
            this.printCount = printCount;
            this.lock = lock;
            this.thisCondition = thisCondition;
            this.nextCondition = nextCondition;
            this.printChar = printChar;
            this.isLastChar = isLastChar;
        }

        @Override
        public void run() {
            lock.lock();
            try{
                for(int i=0;i<printCount;i++){
                    System.out.print(printChar);
                    nextCondition.signal();
                    try {
                        thisCondition.await();
                    } catch (InterruptedException e) {
                    }
                }
                if(isLastChar){
                    System.out.println();
                }
            }finally {
                lock.unlock();
            }

        }
    }


}
