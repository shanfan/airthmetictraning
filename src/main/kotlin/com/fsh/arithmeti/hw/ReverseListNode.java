package com.fsh.arithmeti.hw;


class ListNode{
    ListNode next;
    int value;

    public ListNode(int value) {
        this.value = value;
    }
}

public class ReverseListNode {

    public ListNode ReverseList(ListNode head){
        ListNode newHead = null;
        ListNode p;
        while (head != null){
            p = head.next;
            head.next = newHead;
            newHead = head;
            head = p;
        }
        return newHead;
    }

    private static ListNode head = new ListNode(0);
    static{
        ListNode l1 = new ListNode(1);
        ListNode l2 = new ListNode(2);
        ListNode l3 = new ListNode(3);
        ListNode l4 = new ListNode(4);
        head.next = l1;
        l1.next = l2;
        l2.next = l3;
        l3.next = l4;
    }


    public static void main(String[] args) {
        ReverseListNode r = new ReverseListNode();
        ListNode listNode = r.ReverseList(head);
        while (listNode != null){
            System.out.print(listNode.value+" ");
            listNode = listNode.next;
        }
    }
}
