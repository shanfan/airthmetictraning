package com.fsh.arithmeti.hw;
import java.util.*;
public class DNA {

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            String input = sc.nextLine();
            int length = sc.nextInt();
            int i1 =0,i2=length;
            float minF = Float.MIN_VALUE;
            String minS = "";
            while((i1+length)<input.length()){
                String s = input.substring(i1,i1+length);
                float f = gcRatio(s);
                if(minF < f){
                    minF = f;
                    minS = s;
                }
                i1++;
            }
            System.out.println(minS);
        }
    }

    private static float gcRatio(String s){
        int count =0;
        for(int i=0;i<s.length();i++){
            char c = s.charAt(i);
            char nc = s.charAt(i+1);
            if(c == 'G' || (c == 'C')){
                count++;
            }
        }
        return count*1.0f/s.length();
    }
}
