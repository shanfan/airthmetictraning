package com.fsh.arithmeti.hw;

import java.util.*;

public class FindBrotherWords {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            String input = sc.nextLine();
            String[] ia = input.split(" ");
            int dirCount = Integer.parseInt(ia[0]);
            int brotherIndex = Integer.parseInt(ia[ia.length-1]);
            String searchKey = ia[ia.length-2];
            String[] brother = new String[ia.length-3];
            int bIndex = 0;
            for(int i=1;i<dirCount+1;i++){
                String words = ia[i];
                if(isBrotherWords(words,searchKey)){
                    brother[bIndex++] = words;
                }
            }
            Arrays.sort(brother, new Comparator<String>() {
                public int compare(String o1, String o2) {
                    return o1.compareTo(o2);
                }
            });
            if(bIndex > 0){
                System.out.println(bIndex);
                System.out.println(brother[brotherIndex-1]);
            }else{
                System.out.println(0);
                System.out.println();
            }

        }
    }

    private static boolean isBrotherWords(String s1,String s2){
        if(s1.length() != s2.length()){
            return false;
        }
        if(s1.equals(s2)){
            return false;
        }
        s1 = sort(s1);
        s2 = sort(s2);
        return s1.equals(s2);
    }

    private static String sort(String input){
        char[] sort = new char[input.length()];
        input.getChars(0,input.length(),sort,0);
        Arrays.sort(sort);
        return new String(sort);
    }
}
