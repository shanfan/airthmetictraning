package com.fsh.arithmeti.string

/**
 * 题目：最后一个单词的长度
 * 给定一个仅包含大小写字母和空格 ' ' 的字符串 s，返回其最后一个单词的长度。如果字符串从左向右滚动显示，那么最后一个单词就是最后出现的单词
 *
 * 示例：
 * 输入: "Hello World"
 * 输出: 5
 *
 * 说明： 一个单词是指仅由字母组成、不包含任何空格字符的 最大子字符串
 */
object LengthOfLastWord {
    /**
     * 解题思路：倒叙读取字符，如果开始是空格，则跳过，直到遇见字母，接着遇到第一个空格结束，就是最后一个单词长度
     */
    fun len(s:String):Int{
        var isBegin = false
        var result = 0
        for(i in s.length-1 downTo 0){
            val c = s[i]
            if(s[i].toInt() in 65 .. 90 || s[i].toInt() in 97 .. 122){
                isBegin = true
                result++
            }else{
                if(s[i] != ' '){
                    isBegin = false
                    result = 0
                }
            }
            if(isBegin && s[i] == ' '){
                return result
            }
        }
        return result
    }
}

fun main(args: Array<String>) {
    val a = "abdef abcdefa abcef a"
    println(LengthOfLastWord.len(a))
}