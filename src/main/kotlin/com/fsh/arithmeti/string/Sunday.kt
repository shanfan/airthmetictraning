package com.fsh.arithmeti.string

/**
 * 字符串匹配Sunday算法
 * Sunday 算法是 Daniel M.Sunday 于1990年提出的字符串模式匹配。
 * 其核心思想是：在匹配过程中，模式串发现不匹配时，算法能跳过尽可能多的字符以进行下一步的匹配，从而提高了匹配效率。
 * SUNDAY算法，我们从头部开始比较，一旦发现不匹配，直接找到主串中位于模式串后面的第一个字符
 * 找到了模式串后的第一个字符 ，接下来该怎么做？我们需要查看模式串中是否包含这个元素，如果不包含那就可以跳过一大片，
 * 从该字符的下一个字符开始比较。
 * 如果仍然不匹配，我们继续重复上面的过程。找到模式串的下一个元素
 *
 * Sunday思路：https://www.geekxh.com/1.3.%E5%AD%97%E7%AC%A6%E4%B8%B2%E7%B3%BB%E5%88%97/303.html#_02%E3%80%81sunday-%E5%8C%B9%E9%85%8D
 */
object Sunday{
    fun strStr(origin:String,aim:String):Int{
        //如果origin长度小于aim返回-1
        if(origin.length < aim.length) return -1
        //目标串匹配索引
        var originIndex =0
        //模式串匹配索引
        var aimIndex = 0
        val originArray = origin.toCharArray()
        val aimArray = aim.toCharArray()
        while (aimIndex < aimArray.size){
            //匹配到最后一位都没有匹配上，直接返回-1
            if(originIndex > originArray.size-1){
                return -1
            }
            if(originArray[originIndex] == aimArray[aimIndex]){
                originIndex++
                aimIndex++
            }else{
                //下一次匹配索引值 第一次计算值为6，第二次值为13
                var nextCharIndex = originIndex - aimIndex + aimArray.size
                //下一个目标字符
                if(nextCharIndex < originArray.size){
                    //判断目标字符在模式串中匹配到的位置，返回最后一个匹配的index
                    var step = aimArray.lastIndexOf(originArray[nextCharIndex])
                    if(step == -1){
                        originIndex = nextCharIndex +1
                    }else{
                        originIndex = nextCharIndex -step
                    }
                    //模式串总是从0开始匹配
                    aimIndex =0
                }else{
                    return -1
                }
            }
        }
        return originIndex - aimIndex
    }
}

fun main(args: Array<String>) {
    val origin = "hello girl world!"
    val aim = "boy"
    val index = Sunday.strStr(origin, aim)
    println("$aim in $origin $index")
}