package com.fsh.arithmeti.string

/**
 * 题目：字符串中的第一个唯一字符
 * 给定一个字符串，找到它的第一个不重复的字符，并返回它的索引。如果不存在，则返回 -1。
 *
 * 示例：
 * s = "leetcode"
 * 返回 0
 * s = "loveleetcode"
 * 返回 2
 *
 * 提示：你可以假定该字符串只包含小写字母。
 *
 * 链接：https://leetcode-cn.com/problems/first-unique-character-in-a-string
 */
object FirstUniqChar {
    /**
     * 方法1，暴力解决
     * 使用HashMap 存索引值
     * LikedHashSet存唯一值得索引
     * 找到之后，直接返回LikedHashSet的第一个元素
     */
    fun find(s:String):Int{
        val countMap = HashMap<Char,Int>()
        val indexMap = LinkedHashSet<Int>()
        val charArray = s.toCharArray()
        for(index in charArray.indices){
            if(countMap.containsKey(charArray[index])){
                indexMap.remove(countMap[charArray[index]])
            }else{
                countMap[charArray[index]] = index
                indexMap.add(index)
            }
        }
        return indexMap.first()
    }

    /**
     * 方法2：
     * 使用一个数组存储字符串中[char]最后一次出现的位置
     * 第二次循环如果字符串中的[char]的索引和数组中的值相等，则为目标数
     * 否则就置为-1
     */
    fun find2(s:String):Int{
        val array = arrayOfNulls<Int>(26)
        val charArray = s.toCharArray()
        for(index in charArray.indices){
            array[charArray[index] - 'a'] = index
        }
        for(index in charArray.indices){
            if(index == array[charArray[index]-'a']){
                return index
            }else{
                array[charArray[index] - 'a'] = -1
            }
        }
        return -1
    }
}

fun main(args: Array<String>) {
    val find = FirstUniqChar.find("leetcode")
    println(find)
    val find1 = FirstUniqChar.find("loveleetcode")
    println(find1)
    val find2 = FirstUniqChar.find2("loveleetcode")
    println(find2)
}