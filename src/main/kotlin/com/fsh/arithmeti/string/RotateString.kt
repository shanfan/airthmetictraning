package com.fsh.arithmeti.string

object RotateString {
    fun rotateString(a:String,b:String):Boolean{
        if(a.length != b.length) return false
        if(a == b) return true
        for(index in 0 until a.length){
            val temp = a.substring(index,a.length) + a.substring(0,index)
            if(temp == b){
                return true
            }
        }
        return false
    }
}

fun main(args: Array<String>) {
    val a = "abcde"
    val b = "cdeab"
    val rotateString = RotateString.rotateString(a, b)
    println(rotateString)
}