package com.fsh.arithmeti.string

/**
 * 题目：反转字符串
 * 编写一个函数，其作用是将输入的字符串反转过来。输入字符串以字符数组 char[] 的形式给出。
 * 不要给另外的数组分配额外的空间，你必须原地修改输入数组、使用 O(1) 的额外空间解决这一问题。
 *
 * 你可以假设数组中的所有字符都是 ASCII 码表中的可打印字符。
 *
 * 示例 1：
 * 输入：["h","e","l","l","o"]
 * 输出：["o","l","l","e","h"]
 *
 * 链接：https://leetcode-cn.com/problems/reverse-string
 *
 */
object ReverseString {
    /**
     * 解题思路：双指针
     * [left]和[right]两个指针从左右两端开始向中心移动，每移动一位，就替换一次内容
     * 直到[left]>=[right]为止
     */
    fun reverse(s:CharArray){
        var left =0
        var right = s.size-1
        while (left < right){
            val temp = s[left]
            s[left] = s[right]
            s[right] = temp
            left++
            right--
        }
    }
}

fun main(args: Array<String>) {
    val array = charArrayOf('h','e','l','l','o',',','w','o','r','l','d','!')
    ReverseString.reverse(array)
    println(array.toList().joinToString(""))
}