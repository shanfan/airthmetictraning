package com.fsh.arithmeti.string

/**
 * 题目：打印从1到最大的n位数
 * 输入数字 n，按顺序打印出从 1 到最大的 n 位十进制数。比如输入 3，则打印出 1、2、3 一直到最大的 3 位数 999
 *
 * 示例 1:
 * 输入: n = 1
 * 输出: [1,2,3,4,5,6,7,8,9]
 *
 * 说明：
 * 用返回一个整数列表来代替打印
 * n 为正整数
 */
object PrintNumbers {
    /**
     * 使用pow函数，获取到最大需要打印的10^n-1数值
     */
    fun print(n:Int):IntArray{
        val size = Math.pow(10.0,n.toDouble()).toInt()
        val result = IntArray(size-1)
        for(index in 1 until size){
            result[index-1] = index
        }
        return result
    }

    /**
     * 不使用pow函数
     */
    fun print1(n:Int):IntArray{
        var size = 0
        for(i in 0 until n){
            size = size *10 +9
        }
        val result = IntArray(size)
        for(index in 1 until size+1){
            result[index-1] = index
        }
        return result
    }

    /**
     *这道题目的名字叫做大数打印，如果阈值超出long类型，该怎么办呢？请手动实现一下！
     */
    fun print2(n:Int):CharArray{
        val number = CharArray(n)
        number.forEachIndexed { index, c ->
            number[index] = '0'
        }
        while (incrementNumber(number)){
            saveNumber(number)
        }
        return number
    }

    private fun incrementNumber(number:CharArray):Boolean{
        //是否退出循环
        var isBreak = false
        //进位标志
        var carryFlag = 0
        var len = number.size
        for(index in len-1 .. 0){
            var sum = number[index] - '0' + carryFlag
            if(index == len -1){
                ++sum
            }
            if(sum >= 10){
                if(index == 0){
                    isBreak = true
                }else{
                    sum -= 10
                    carryFlag = 1
                    number[index] = (sum + '0'.toInt()).toChar()
                }
            }else{
                number[index] = (sum + '0'.toInt()).toChar()
                break
            }
        }
        return isBreak
    }

    private fun saveNumber(number: CharArray){
        var isBegin0 = true
        for(c  in number){
            isBegin0 = isBegin0 && c != '0'
            if(!isBegin0){
                print(c)
            }
        }
        println()
    }
}

fun main(args: Array<String>) {
    val n = 3
    var result = PrintNumbers.print(n)
    println(result.toList())
    result = PrintNumbers.print1(n)
    println(result.toList())
    val res = PrintNumbers.print2(4)
    println(res.toList())
}