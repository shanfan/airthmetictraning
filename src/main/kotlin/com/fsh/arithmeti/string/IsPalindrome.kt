package com.fsh.arithmeti.string

/**
 * 验证回文串
 * 给定一个字符串，验证它是否是回文串，只考虑字母和数字字符，可以忽略字母的大小写。
 * 说明：本题中，我们将空字符串定义为有效的回文串。
 *
 *
 *
 * 示例 1:
 *
 * 输入: "A man, a plan, a canal: Panama"
 * 输出: true
 *
 * 说明：“回文串”是一个正读和反读都一样的字符串，比如“level”或者“noon”等等就是回文串
 *
 */
object IsPalindrome{
    /**
     * 解题思路：使用双指针，判断两个指针对应的字符是否相等
     * 注意：
     *   需要判断一下是否是数字或者字母，不是就需要指针移动（左边不是，左边移动；右边不是，右边移动）
     *   字母的话，同一使用大写或者小写来判断
     *
     */
    fun isPalindrome(s:String):Boolean{
        var left = 0
        var right = s.length-1
        var result:Boolean = true
        while (left < right){
            var l = s[left]
            var r = s[right]
            if(!isAvailable(l)){
                left++
                continue
            }
            if(!isAvailable(r)){
                right--
                continue
            }
            l = toLowerCase(l)
            r = toLowerCase(r)
            if(l == r){
                left ++
                right--
                continue
            }else{
                return false
            }
        }
        return result
    }

    private fun isAvailable(char:Char):Boolean{
        val i = char.toInt()
        return i in 48 .. 57 || i in 65 .. 90 || i in 97 .. 122
    }

    private fun toLowerCase(char: Char):Char{
        val i = char.toInt()
        //大写范围
        if( i in 65 .. 90){
            return (i+32).toChar()
        }
        return char
    }
}

fun main(args: Array<String>) {
    println(IsPalindrome.isPalindrome("A man, a plan, a canal: Panama"))
    println(IsPalindrome.isPalindrome("race a car"))

}