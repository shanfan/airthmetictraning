package com.fsh.arithmeti.utils

fun HashMap<*,*>.getTable():Array<Any>{
    return try{
        val declaredField = javaClass.getDeclaredField("table")
        declaredField.isAccessible = true
        declaredField.get(this) as Array<Any>
    }catch (e:Exception){
        emptyArray()
    }
}