package com.fsh.arithmeti.utils

import java.lang.reflect.Field
import java.util.*
import javax.swing.tree.TreeNode

object NodePrintFactory{

    fun printTable(any:Array<Any>){
        any.forEachIndexed{index,value->
            if("java.util.HashMap\$Node" == value?.javaClass?.name){
                printNode(index,value)
            }else if("java.util.HashMap\$TreeNode" == value?.javaClass?.name){
                printTreeNode(index,value)
            }
        }
    }

    private fun printNode(index:Int,node: Any){
        var n:Any? = node
        val ssb = StringBuffer("table[$index]=")
        while (n != null){
            try{

                val fields = node.javaClass.declaredFields.filter { it.name != "next" }.map {
                    it.isAccessible = true
                    "${it.name}:${it.get(node)}"
                }.joinToString(",")
                ssb.append("[$fields]")
                val declaredField = node.javaClass.getDeclaredField("next").let {
                    it.isAccessible = true
                    it
                }
                n = declaredField.get(n)
                if(n != null){
                    ssb.append("-->")
                }
            }catch (e:Exception){
            }
        }
        println(ssb)
    }

    private fun printTreeNode(index: Int,treeNode:Any){
        val stack  = Stack<Any>()
        stack.push(treeNode)
        val ssb = StringBuffer("table[$index]=[")
        while (stack.isNotEmpty()){
            var size = stack.size

            while (size-- > 0){
                val node = stack.pop()

                val left = getTreeNodeFieldValue("left", node)
                val right = getTreeNodeFieldValue("right",node)
                val key = getTreeNodeFieldValue("key",node)
                val value = getTreeNodeFieldValue("value",node)
                ssb.append("key:$key,value:$value")
                if(left != null){
                    stack.push(left)
                }
                if(right != null){
                    stack.push(right)
                }
            }
            ssb.append("\n")
            println(ssb)
        }
        ssb.append("]")
    }

    private fun getTreeNodeFieldValue(propertyName:String,treeNode: Any):Any?{
        val declaredField = getField(propertyName,treeNode.javaClass)
        return declaredField?.get(treeNode)
    }

    private fun getField(propertyName: String,clazz:Class<*>):Field?{
        try{
            if(clazz.name == "java.lang.Object"){
                return null
            }
            if(clazz.isInterface){
                return null
            }
            return clazz.getDeclaredField(propertyName).apply{
                isAccessible = true
            }
        }catch (e:NoSuchFieldException){
            return getField(propertyName,clazz.superclass)
        }
    }
}