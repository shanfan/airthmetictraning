package com.fsh.arithmeti.utils

class HData(var name:String,var age:Int) {

    override fun hashCode(): Int {
        return 10
    }

    override fun equals(other: Any?): Boolean {
        if(other !is HData){
            return false
        }
        return name == other.name && age == other.age
    }

    override fun toString(): String {
        return "{\"name\":\"$name\",\"age\":$age}"
    }
}