package com.fsh.arithmeti.btree

import sun.reflect.generics.tree.Tree
import java.util.*
import kotlin.math.max

/**
 * 二叉树：前序、中序、后序访问
 */
object VisitTreeNode{

    //前序访问：根-左-右
    fun firstOrder(node:TreeNode<*>?){
        if(node == null){
            return
        }
        print(node.value)
        firstOrder(node.left)
        firstOrder(node.right)
    }

    //中序访问：左-根-右
    fun inOrder(node: TreeNode<*>?){
        if(node == null){
            return
        }
        inOrder(node.left)
        print(node.value)
        inOrder(node.right)
    }

    //后序访问：
    fun lastOrder(node: TreeNode<*>?){
        if(node == null){
            return
        }
        inOrder(node.left)
        inOrder(node.right)
        print(node.value)
    }
    //深度优先获取二叉树深度
    fun treeLevel(node:TreeNode<*>?):Int{
        if(node == null) return 0
        val stack:Stack<TreeNode<*>> = Stack()
        stack.push(node)
        val levelS:Stack<Int> = Stack()
        levelS.push(1)
        var level = 1
        while (stack.isNotEmpty()){
            val n = stack.pop()
            val l = levelS.pop()
            level = max(level,l)
            if(n.left != null){
                stack.push(n.left)
                levelS.push(l+1)
            }
            if(n.right != null){
                stack.push(n.right)
                levelS.push(l+1)
            }
        }
        return level
    }
    //广度优先
    fun treeLevelB(node: TreeNode<*>?):Int{
        if(node == null) return 0
        val stack:Stack<TreeNode<*>> = Stack()
        stack.push(node)
        var level = 0
        while (stack.isNotEmpty()){

            var size = stack.size
            while (size-- > 0){
                val n = stack.pop()
                if(n.left != null){
                    stack.push(n.left)
                }
                if(n.right != null){
                    stack.push(n.right)
                }
            }
            level++
        }
        return level
    }
    fun kthNode(root:TreeNode<*>?,k:Int):TreeNode<*>?{
        if(root == null) return null
        var pR = root
        var count = 0
        val stack = Stack<TreeNode<*>>()
        while (pR != null || stack.isNotEmpty()){
            while (pR != null){
                stack.push(pR)
                pR = pR.left
            }
            pR = stack.pop()
            count++
            if(count == k) return pR
            pR = pR?.right
        }
        return null
    }
}

fun main(args: Array<String>) {
    val treeNode = generateTreeNode()
    VisitTreeNode.firstOrder(treeNode)
    println()
    VisitTreeNode.inOrder(treeNode)
    println()
    VisitTreeNode.lastOrder(treeNode)
    println()
    println("TreeLevel By DFS --> ${VisitTreeNode.treeLevel(treeNode)}")
    println("TreeLevel By BFS --> ${VisitTreeNode.treeLevelB(treeNode)}")

    val treeNode1 = generateBalancedTree()
    println("SearchTree ---> ${VisitTreeNode.kthNode(treeNode1,3)}")
}