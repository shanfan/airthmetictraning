package com.fsh.arithmeti.btree

import java.util.*

/**
 * 题目：获取二叉树最大深度
 * 采用BFS(Breadth-First Search)广度优先
 */
object BFSMaxDepth {

    fun bfs(treeNode: TreeNode<*>):Int{
        val stack = Stack<TreeNode<*>>()
        var count = 0
        stack.push(treeNode)
        while (stack.isNotEmpty()){
            var size = stack.size
            while (size-- > 0){
                val node = stack.pop()
                if(node.left != null){
                    stack.push(node.left)
                }
                if(node.right != null){
                    stack.push(node.right)
                }
            }
            count ++
        }
        return count
    }
}

fun main(args: Array<String>) {
    val treeNode = generateTreeNode()
    println(BFSMaxDepth.bfs(treeNode))
}