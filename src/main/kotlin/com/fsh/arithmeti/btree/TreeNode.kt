package com.fsh.arithmeti.btree

/**
 * 二叉树
 */
data class TreeNode<T>(var value: T, var left: TreeNode<T>? = null, var right: TreeNode<T>? = null) {
    override fun toString(): String {
        return value.toString()
    }
}

fun generateTreeNode(): TreeNode<Char> {
    val left = TreeNode('b', TreeNode('d'), TreeNode('e'))
    val right = TreeNode('c', TreeNode('f'), TreeNode('g', TreeNode('x')))
    return TreeNode('a', left, right)
}

fun generateIntTreeNode(): TreeNode<Int> {
    val left = TreeNode(5, TreeNode(1), TreeNode(6))
    val right = TreeNode(35, TreeNode(29, TreeNode(19), TreeNode(30)), TreeNode(38))
    return TreeNode(10,left,right)
}

fun generateBalancedTree():TreeNode<Int>{
    val left = TreeNode(5,TreeNode(3, TreeNode(1), TreeNode(2)), TreeNode(4,TreeNode(2), TreeNode(3)))
    val right = TreeNode(7, TreeNode(6),TreeNode(9))
    return TreeNode(10,left,right)
}

