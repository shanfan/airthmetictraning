package com.fsh.arithmeti.btree

import java.util.*

/**
 * 题目：获取二叉树的高度
 * 采用了DFS（Deep-First-Search）深度优先算法
 */
object MaxDepth {
    /**
     * 1.递归
     */
    fun dfs(node:TreeNode<*>?):Int{
        if(node == null){
            return 0
        }
        return max(dfs(node.left), dfs(node.right)) + 1
    }

    private fun max(a:Int,b:Int):Int{
        return if(a > b) a else b
    }

    fun traversal(node: TreeNode<*>):List<TreeNode<*>>{
        val result = ArrayList<TreeNode<*>>()
        val stack = Stack<TreeNode<*>>()
        stack.add(node)

        while (stack.isNotEmpty()){
            val n = stack.pop()
            result.add(n)
            if(n.right != null){
                stack.push(n.right)
            }
            if(n.left != null){
                stack.push(n.left)
            }
        }
        return result
    }

    /**
     * 非递归
     */
    fun dfs1(node: TreeNode<*>):Int{
        var result = 0
        val stack  = Stack<TreeNode<*>>()
        stack.push(node)
        val level = Stack<Int>()
        level.push(1)
        while (stack.isNotEmpty()){
            val tempLevel = level.pop()
            val n = stack.pop()
            result = max(result,tempLevel)
            if(n.left != null){
                stack.push(n.left)
                level.push(tempLevel+1)
            }
            if(n.right != null){
                stack.push(n.right)
                level.push(tempLevel+1)
            }
        }
        return result
    }
}

fun main(args: Array<String>) {
    val treeNode = generateTreeNode()
    println(MaxDepth.dfs(treeNode))
    println(MaxDepth.dfs1(treeNode))
    println(MaxDepth.traversal(treeNode))
}