package com.fsh.arithmeti.btree;

import java.util.HashMap;

import static com.fsh.arithmeti.btree.TreeNodeKt.generateIntTreeNode;

class DeleteNode{


    public TreeNode<Integer> delete(TreeNode<Integer> node,int value){
        if(node == null){
            return null;
        }
        return helper(node,value);
    }

    private TreeNode<Integer> helper(TreeNode<Integer> node,int value){
        if(node == null){
            return null;
        }
        if(node.getValue() > value){
          node.setLeft(helper(node.getLeft(),value));
        }else if(node.getValue() < value){
            node.setRight(helper(node.getRight(),value));
        }else{
            if(node.getLeft() == null) return node.getRight();
            if(node.getRight() == null) return node.getLeft();
            TreeNode t = node;
            node = min(t.getRight());
            node.setRight(deleteMin(t.getRight()));
            node.setLeft(t.getLeft());
        }

        return node;
    }

    private TreeNode<Integer> min(TreeNode<Integer> node){
        if(node.getLeft() == null) return node;
        return min(node.getLeft());
    }

    private TreeNode<Integer> deleteMin(TreeNode<Integer> node){
        if(node.getLeft() == null) return node.getRight();
        node.setLeft(deleteMin(node.getLeft()));
        return node;
    }


    public static void main(String[] args) {
        DeleteNode d = new DeleteNode();
        TreeNode<Integer> treeNode = generateIntTreeNode();
        TreeNode<Integer> delete = d.delete(treeNode, 19);
        System.err.println("dddddddddddd");
    }
}
