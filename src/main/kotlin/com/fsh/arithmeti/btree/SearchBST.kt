package com.fsh.arithmeti.btree

/**
 * 题目搜索二叉树
 */
object SearchBST {
    /**
     * 递归搜索
     */
    fun search(treeNode: TreeNode<Int>?, value:Int):TreeNode<Int>?{
        return when{
            treeNode == null -> null
            treeNode.value > value -> search(treeNode.left,value)
            treeNode.value < value -> search(treeNode.right,value)
            treeNode.value == value -> treeNode
            else -> null
        }
    }

    /**
     * 遍历搜索
     */
    fun searchBST(treeNode: TreeNode<Int>,value: Int):TreeNode<Int>?{
        var node:TreeNode<Int>? = treeNode
        while (node != null){
            node = when{
                node.value == value -> return node
                node.value > value -> node.left
                else -> node.right
            }
        }
        return null
    }
}

fun main(args: Array<String>) {
    val treeNode = generateIntTreeNode()
    val searchValue = 18//19
    println(SearchBST.search(treeNode,searchValue))
    println(SearchBST.searchBST(treeNode,searchValue))
}