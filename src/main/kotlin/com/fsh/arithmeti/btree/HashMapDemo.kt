package com.fsh.arithmeti.btree

import com.fsh.arithmeti.utils.HData
import com.fsh.arithmeti.utils.NodePrintFactory
import com.fsh.arithmeti.utils.getTable

/**
 * HashMap源码调试
 * 一.put流程分析
 *   1.先通过hash(key)方法计算出key的hashCode,:由于该表使用2的幂次掩码，因此仅在当前掩码上方的位中发生变化的哈希集将始终发生冲突
 *   2.根据key的hashCode找到在table中的索引位置[(size-1) & hash]
 *     a.如果该索引位置的值为null，则生成一个HashMap$Node对象，保存在改位置
 *     b.如果该索引位置的值不为null，且是HashMap$TreeNode(红黑树)，则直接保存到红黑树中
 *     c.其他情况：该索引值得位置不为null，且为HashMap$Node（链表）对象时，
 *       先找到链表的最后一个节点，然后把值加入到最后一个节点后面
 *        c1:如果链表长度（不计新加入节点）  >=7 (TREEIFY_THRESHOLD(8) - 1)，将链表转为红黑数
 *          在链表转TreeNode的时候，如果table的容量小于16时，会先扩容，再转链表,否则就直接加入TreeNode,然后平衡红黑树
 *   3.在保存数据完成之后，会判断数据量>扩容阈值
 *
 * 二.扩容流程分析
 *   如果没有初始化，则使用初始化容器大小(16)和需要扩容的阈值(16*0.75=12)；
 *     如果已经初始化过，先判断容器大小是否是大于了最大容器值2^30，如果大于就将扩容阈值设置为Integer.MAX_VALUE,直接返回oldTab
 *     否则就将容器大小扩大为原来的一倍，然后将扩容阈值扩大为之前的一倍，新开辟一个数组空间，
 *     然后根据之前的Node.hash计算出需要存储在新空间中的索引位置，如果新的索引位置已经有值，就存为链表或者升级为红黑树
 *
 * 三.remove流程分析
 *   1.先hash(key)获取hashCode，然后再(size-1)&hash计算出数据在table中的索引位置
 *   2.然后在Node或者TreeNode中查出需要删除的值(key\hash两个变量相等)
 *   3.然后再去Node或者TreeNode中删除该节点，最后返回node
 *     在TreeNode删除的时候，如果TreeNode的的树高度小于3，则就切换为Node模式
 */
object HashMapDemo {
    private val map:HashMap<HData,HData> = HashMap()
    fun test(){
        /**
         * \u4e00-\u9fa5
         */
        val start = 0
        val end = 10
        for(index in start.. end){
//            val byte = Random.nextBytes(index)
//            val key = "\\u${index.toString(16)}"
            val hData = HData(index.toString(),index)
            if(index - start > 9){
                print("")
            }
            map[hData] = hData
        }
        NodePrintFactory.printTable(map.getTable())
//        map.remove("150")
//        NodePrintFactory.printTable(map.getTable())
    }
}

fun main(args: Array<String>) {
    HashMapDemo.test()
}