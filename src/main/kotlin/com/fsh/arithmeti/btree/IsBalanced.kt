package com.fsh.arithmeti.btree

/**
 * 题目：是否是平衡二叉树
 *  本题中，一棵高度平衡二叉树定义为：
 *    一个二叉树每个节点 的左右两个子树的高度差的绝对值不超过1。
 *
 * 示例 1:

给定二叉树 [3,9,20,null,null,15,7]

3
/ \
9  20
/  \
15   7
返回 true

示例 2:

给定二叉树 [1,2,2,3,3,null,null,4,4]

1
/ \
2   2
/ \
3   3
/ \
4   4
返回 false

来源：力扣（LeetCode）
链接：https://leetcode-cn.com/problems/balanced-binary-tree
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
object IsBalanced {
    /**
     * 解题思路：每个节点左右两边的高度差不大于1既为平衡数
     */
    fun isBalanced(treeNode: TreeNode<Int>?):Boolean{
        if(treeNode == null) return true
        return Math.abs(height(treeNode.left)- height(treeNode.right)) < 2
         && isBalanced(treeNode.left) && isBalanced(treeNode.right)
    }

    private fun height(treeNode: TreeNode<Int>?):Int{
        return if(treeNode == null) 0
        else{
            1 + Math.max(height(treeNode.left), height(treeNode.right))
        }
    }
}

fun main(args: Array<String>) {
    val treeNode = generateBalancedTree()
    println(IsBalanced.isBalanced(treeNode))
}