package com.fsh.arithmeti.btree

/**
 * 题目：验证二叉搜索树
 * 先看定义：二叉搜索树（Binary Search Tree），（又：二叉查找树，二叉排序树）它或者是一棵空树，或者是具有下列性质的二叉树：
 * 若它的左子树不空，则左子树上所有结点的值均小于它的根结点的值；若它的右子树不空，则右子树上所有结点的值均大于它的根结点的值；
 * 它的左、右子树也分别为二叉搜索树
 */
object ValidBST{
    fun isValidBST(treeNode: TreeNode<Int>):Boolean{

        return isBST(treeNode,Int.MIN_VALUE,Int.MAX_VALUE)
    }

    private fun isBST(treeNode: TreeNode<Int>?,min:Int,max:Int):Boolean{
        if(treeNode == null) return true
        if(min > treeNode.value || max <= treeNode.value){
            return false
        }
        return isBST(treeNode.left,min,treeNode.value) && isBST(treeNode.right,treeNode.value,max)
    }
}

fun main(args: Array<String>) {
    val treeNode = generateIntTreeNode()
    println(ValidBST.isValidBST(treeNode))
}