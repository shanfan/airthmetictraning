package com.fsh.arithmeti.btree

/**
 * 题目：完全二叉树的节点个数
 * 说明：完全二叉树的定义如下：在完全二叉树中，除了最底层节点可能没填满外，其余每层节点数都达到最大值，
 * 并且最下面一层的节点都集中在该层最左边的若干位置。若最底层为第 h 层，则该层包含 1~ 2h 个节点
 *
 * 需要掌握第二种方案
 */
object FullTreeNodeCount{
    /**
     * 方案1：暴力解析，遍历所有节点，统计数目
     *
     */
    fun count(treeNode: TreeNode<Int>?):Int{
        if(treeNode != null){
            return 1 + count(treeNode.left) + count(treeNode.right)
        }
        return 0
    }

    /**
     * 方案2：，可以将该完全二叉树可以分割成若干满二叉树和完全二叉树
     * 满二叉树直接根据层高h计算出节点为2^h-1，然后继续计算子树中完全二叉树节点
     *
     * 那如何分割成若干满二叉树和完全二叉树呢？
     *  对任意一个子树，遍历其左子树层高left，右子树层高right，相等左子树则是满二叉树，否则右子树是满二叉树
     *
     */
    fun count1(treeNode: TreeNode<Int>?):Int{
        if(treeNode == null){
            return 0
        }
        val leftHeight = height(treeNode.left)
        val rightHeight = height(treeNode.right)
        if(leftHeight == rightHeight){
            return   (1.shl(leftHeight)) + count1(treeNode.right)
        }
        return  (1.shl(rightHeight)) + count1(treeNode.left)
    }

    /**
     * 获取树的高度
     */
    private fun height(treeNode: TreeNode<Int>?):Int{
        var node = treeNode
        var result = 0
        while (node != null){
            node = node.left
            result++
        }
        return result
    }
}

fun main(args: Array<String>) {
    val treeNode1 = generateBalancedTree()
    var startTime = System.nanoTime()
    var count1 = FullTreeNodeCount.count(treeNode1)
    var endTime = System.nanoTime()
    println("递归耗时：${endTime - startTime}ns,结果$count1")
    startTime = System.nanoTime()
    count1 = FullTreeNodeCount.count1(treeNode1)
    endTime = System.nanoTime()
    println("特性统计：${endTime - startTime}ns,结果$count1")
}