package com.fsh.arithmeti.btree

import com.fsh.arithmeti.utils.HData
import java.util.concurrent.ConcurrentHashMap

object ConcurrentHashMapDemo {
    private val COUNT = 10
    private val map:MutableMap<HData,HData> = ConcurrentHashMap()

    fun test(){
        val start = 0x4e00
        Thread(TR(start),"")
        Thread(TR(start+ COUNT),"")
        Thread(TR(start+ COUNT*2),"")
        Thread(TR(start+ COUNT*3),"")
    }

    class TR(val start:Int) : Runnable{
        override fun run() {
            for(index in start until start+COUNT){
                val data = HData(index.toString(16),index)
                map[data] = data
            }
        }
    }
}

fun main(args: Array<String>) {
    ConcurrentHashMapDemo.test()
}