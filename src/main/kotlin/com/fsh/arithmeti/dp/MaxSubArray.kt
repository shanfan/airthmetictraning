package com.fsh.arithmeti.dp

import kotlin.math.max

/**
 * 题目：最大子序和
 * 给定一个整数数组 nums ，找到一个具有最大和的连续子数组（子数组最少包含一个元素），返回其最大和。
 *
 * 示例:
 * 输入: [-2,1,-3,4,-1,2,1,-5,4]
 * 输出: 6
 * 解释: 连续子数组 [4,-1,2,1] 的和最大，为 6。
 *
 * 链接：https://leetcode-cn.com/problems/maximum-subarray/
 */
object MaxSubArray{
    /**
     * 解题思路：
     * 题目需求最终需要一个最大值，依次遍历数组，相加，获取一个最大值，如果比之前的值小了，直接从新开始
     */
    fun maxSubArray(nums:IntArray):Int{
        var maxValue = Int.MIN_VALUE
        var preVal = 0
        for(index in nums.indices){
            preVal = max(preVal+nums[index],nums[index])
            maxValue = max(preVal,maxValue)
        }
        return maxValue
    }
}

fun main(args: Array<String>) {
    val nums = intArrayOf(-2,1,-3,4,-1,2,1,-5,4)
    val result1 = MaxSubArray.maxSubArray(nums)

    println("result1 ----> $result1")
}