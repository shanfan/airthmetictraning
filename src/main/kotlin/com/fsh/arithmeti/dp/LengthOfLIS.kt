package com.fsh.arithmeti.dp

/**
 * 题目：最长上升子序列
 * 给定一个无序的整数数组，找到其中最长上升子序列的长度
 *
 * 示例:
 *
 * 输入: [10,9,2,5,3,7,101,18]
 * 输出: 4
 * 解释: 最长的上升子序列是 [2,3,7,101]，它的长度是 4。
 *
 * 说明:
 * 可能会有多种最长上升子序列的组合，你只需要输出对应的长度即可。
 * 你算法的时间复杂度应该为 O(n2) 。
 *
 * 链接：https://leetcode-cn.com/problems/longest-increasing-subsequence
 *
 */
object LengthOfLIS {
    /**
     * 解题思路：动态规划
     * 使用dp[i]表示最长上升子序列长度（表示数组的从第0位到第i位的最长上升子序列长度）
     * 需要双重循环，从0-length为外层循环，0-i为内循环，记录出0-i的最长上升子序列长度，然后取个最大值
     */
    fun lengthOfLIS(nums: IntArray): Int {
        val list = arrayOfNulls<Int>(nums.size)
        var result = 0
//        for(value in nums){
//            if(value > result[i]?:Int.MIN_VALUE){
//                result[i] = value
//            }else if(value < result[i]?:Int.MIN_VALUE){
//                result[i] = value
//            }
//        }

        for(index in nums.indices){
            list[index] = 1
            for(j in 0 until index){
                if(nums[j] < nums[index]){
                    list[index] = max(list[j]!!.plus(1),list[j]!!)
                }
            }
            result = max(list[index]!!,result)
        }

        return result
    }

    private fun max(i1:Int,i2:Int):Int =
            if(i1 > i2) i1 else i2
}

fun main(args: Array<String>) {
    // [10,9,2,5,3,7,101,18]
    val nums = intArrayOf(1,2,3,4,0,1,2,3,4,5,6)
    val lengthOfLIS = LengthOfLIS.lengthOfLIS(nums)
    println(lengthOfLIS)
}