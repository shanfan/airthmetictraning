package com.fsh.arithmeti.dp

/**
 * 题目：三角形最小路径和
 * 给定一个三角形，找出自顶向下的最小路径和。每一步只能移动到下一行中相邻的结点上。
 * 相邻的结点 在这里指的是 下标 与 上一层结点下标 相同或者等于 上一层结点下标 + 1 的两个结点
 *
 *
 * 例如，给定三角形：
 * [
 *    [2],
 *   [3,4],
 *  [6,5,7],
 * [4,1,8,3]
 * ]
 *
 * 自顶向下的最小路径和为 11（即，2 + 3 + 5 + 1 = 11）。
 *
 * 说明：
 *
 * 如果你可以只使用 O(n) 的额外空间（n 为三角形的总行数）来解决这个问题，那么你的算法会很加分
 *
 * 链接：https://leetcode-cn.com/problems/triangle
 *
 *
 */
object MinimumTotal{
    /**
     * 解题思路：
     */
    fun minimumTotal(triangle: List<List<Int>>): Int{
        val f = Array(triangle.size) { IntArray(triangle.size) }
        f[0][0] = triangle[0][0]
        for(i in 1 until triangle.size){
            f[i][0] = f[i-1][0] + triangle[i][0]
            for(j in 1 .. i){
                f[i][j] = min(f[i-1][j-1],f[i-1][j]) + triangle[i][j]
            }
            f[i][i] = f[i-1][i-1] + triangle[i][i]
        }
        var minElement = f[triangle.size-1][0]
        for(va in f[triangle.size-1]){
            minElement = min(minElement,va)
        }
        return minElement
    }

    fun min(i1:Int,i2:Int):Int{
        return if(i1 > i2) i2 else i1
    }
}

fun main(args: Array<String>) {
    val list:ArrayList<ArrayList<Int>> = ArrayList()
    val list1 = arrayListOf(2)
    val list2 = arrayListOf(3,4)
    val list3 = arrayListOf(6,5,7)
    val list4 = arrayListOf(4,1,8,3)
//    val list1 = arrayListOf(-1)
//    val list2 = arrayListOf(2,3)
//    val list3 = arrayListOf(1,-1,-3)

    list.add(list1)
    list.add(list2)
    list.add(list3)
    list.add(list4)
    val minimumTotal = MinimumTotal.minimumTotal(list)
    println(minimumTotal)
}