package com.fsh.arithmeti.dp

/**
 * 题目：爬楼梯
 * 假设你正在爬楼梯。需要 n 阶你才能到达楼顶。
 * 每次你可以爬 1 或 2 个台阶。你有多少种不同的方法可以爬到楼顶呢？
 *
 * 注意：给定 n 是一个正整数。
 *
 * 示例 1：
 * 输入： 2
 * 输出： 2
 *
 * 解释： 有两种方法可以爬到楼顶。
 *  1.  1 阶 + 1 阶
 *  2.  2 阶
 *
 * 示例 2：
 * 输入： 3
 * 输出： 3
 *
 * 解释： 有三种方法可以爬到楼顶。
 *   1.  1 阶 + 1 阶 + 1 阶
 *   2.  1 阶 + 2 阶
 *   3.  2 阶 + 1 阶
 *
 *   链接：https://leetcode-cn.com/problems/climbing-stairs
 *
 *   题目分析：
 *
 *   爬 1 阶楼梯 有 1 种方法
 *   爬 2 阶楼梯 有 2 种方法
 *   爬 3 阶楼梯 有 3 种方法
 *   爬 4 阶楼梯 有 5 种方法
 *   那么爬n阶楼梯 有 爬第n-1阶楼梯的方法数 + 爬第n-2阶楼梯的方法数
 */
object ClimbStairs {
    /**
     * 方法1：根据题目分析，可以采用递归方法
     */
    fun climb1(n: Int): Int =
            when (n) {
                1 -> 1
                2 -> 2
                else -> climb1(n - 1) + climb1(n - 2)
            }

    /**
     * 动态规划分析：
     *   爬第一阶有1种方法
     *   爬第二阶有2种方法
     *   后续更高的楼梯方法数 = 爬前面两个阶的方法数和
     *  可以把问题归类为三个问题，
     *   1. 1阶楼梯
     *   2. 2阶楼梯
     *   3. n阶楼梯
     *
     */
    fun climb2(n:Int):Int {
        if(n <=2 ){
            return n
        }
        val dp = IntArray(n)
        dp[0] = 1
        dp[1] = 2
        for(i in 2 until n){
            dp[i] = dp[i-1] + dp[i-2]
        }
        return dp[n-1]
    }

}

fun main(args: Array<String>) {
    val flowCount = 20
    var startTime = System.currentTimeMillis()
    val count = ClimbStairs.climb1(flowCount)
    var endTime = System.currentTimeMillis()
    println("递归算法计算爬 $flowCount 楼有 $count 方法耗时:${endTime-startTime}ms")
    startTime = System.currentTimeMillis()
    val count2 = ClimbStairs.climb2(flowCount)
    endTime = System.currentTimeMillis()
    println("动态规划算法计算爬 $flowCount 楼有 $count2 方法耗时:${endTime-startTime}ms")
}