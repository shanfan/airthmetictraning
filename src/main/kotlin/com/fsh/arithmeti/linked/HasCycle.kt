package com.fsh.arithmeti.linked

/**
 * 题目：环形链表
 * 给定一个链表，判断链表中是否有环。
 *
 * 为了表示给定链表中的环，我们使用整数 pos 来表示链表尾连接到链表中的位置（索引从 0 开始）。 如果 pos 是 -1，则在该链表中没有环。
 *
 * 示例 1：
 * 输入：head = [3,2,0,-4], pos = 1
 * 输出：true
 *
 * 解释：链表中有一个环，其尾部连接到第二个节点。
 *
 *
 * 示例 2：
 * 输入：head = [1,2], pos = 0
 * 输出：true
 *
 * 解释：链表中有一个环，其尾部连接到第一个节点。
 *
 *
 * 示例 3：
 * 输入：head = [1], pos = -1
 * 输出：false
 *
 * 解释：链表中没有环。
 *
 *
 * 进阶：
 * 你能用 O(1)（即，常量）内存解决此问题吗？
 *
 *
 * 链接：https://leetcode-cn.com/problems/linked-list-cycle
 */
object HasCycle{
    /**
     * 思路1：利用hash表来存储已经访问过的节点，如果已经访问过的节点再次被访问到了，直接返回true
     */
    fun hasCycle(listNode: SinglyListNode?):Boolean{
        val hash = HashSet<SinglyListNode>()
        var p = listNode
        while (p != null){
            if(hash.contains(p)){
                return true
            }else{
                hash.add(p)
                p = p.next
            }
        }
        return false
    }

    /**
     * 思路2：采用双指针处理，慢指针每次走一步，快指针每次两步，如果两个指针汇合，则有环
     * 否则不为环
     */
    fun hasCycle1(listNode: SinglyListNode?):Boolean{
        var slow = listNode
        var fast = listNode?.next
        while (slow != fast){
            if(slow == null || fast == null){
                return false
            }
            slow = slow.next
            fast = fast.next?.next
        }
        return true
    }
}

fun main(args: Array<String>) {
    //带环的链表
    val cycleLinkedNode = generateCycleSinglyLinkedNode()
    //不带环的链表
    val linkedNode = generateSingleListNode()

    val r1 = HasCycle.hasCycle(cycleLinkedNode)
    val r2 = HasCycle.hasCycle1(cycleLinkedNode)

    val r3 = HasCycle.hasCycle(linkedNode)
    val r4 = HasCycle.hasCycle(linkedNode)

    println("r1=[$r1],r2=[$r2],r3=[$r3],r4=[$r4]")
}