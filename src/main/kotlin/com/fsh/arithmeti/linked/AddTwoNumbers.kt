package com.fsh.arithmeti.linked

/**
 * 题目：两数相加
 *
 * 给出两个 非空 的链表用来表示两个非负的整数。其中，它们各自的位数是按照 逆序 的方式存储的，并且它们的每个节点只能存储 一位 数字。
 * 如果，我们将这两个数相加起来，则会返回一个新的链表来表示它们的和。
 * 您可以假设除了数字 0 之外，这两个数都不会以 0 开头。
 *
 * 示例：
 *
 * 输入：(2 -> 4 -> 3) + (5 -> 6 -> 4)
 * 输出：7 -> 0 -> 8
 * 原因：342 + 465 = 807
 *
 * 链接：https://leetcode-cn.com/problems/add-two-numbers
 *
 */
object AddTwoNumbers {
    /**
     * 解题思路：根据题意，链表中存储的数据是倒叙的，从右到左是低位到高位
     * 只需要依次将各位的数据加起来
     *   如果 > 9 则需要进位 [addon]=1
     *   否则 就不需要进位 [addon] =0
     * 需要考虑两个链表长度不一致时，直接把后续的值给加入进来
     */
    fun add(list1: SinglyListNode?, list2: SinglyListNode?): SinglyListNode? {
        var l1 = list1
        var l2 = list2
        val result = SinglyListNode(0, null)
        var head = result
        var addon = 0
        while (l1 != null || l2 != null) {
            val l1Value = l1?.value ?: 0
            val l2Value = l2?.value ?: 0
            head.value = (l1Value + l2Value + addon)%10
            addon = if(l1Value + l2Value + addon > 9){
                1
            }else{
                0
            }
            l1 = l1?.next
            l2 = l2?.next
            if(l1 != null || l2 != null){
                head.next = SinglyListNode(0,null)
                head = head.next!!
            }
        }

        if(addon == 1){
            head.next = SinglyListNode(1,null)
        }
        return result
    }
}

fun main(args: Array<String>) {
    val l1 = generateSinglyNode(9,9,9)
    val l2 = generateSinglyNode(9,9,9)
    val add = AddTwoNumbers.add(l1, l2)
    println("add result --> $add")
}