package com.fsh.arithmeti.linked

/**
 *  题目：删除链表的倒数第N个节点
 *
 *  给定一个链表，删除链表的倒数第 n 个节点，并且返回链表的头结点。
 *
 *  示例：
 *
 *  给定一个链表: 1->2->3->4->5, 和 n = 2.
 *  当删除了倒数第二个节点后，链表变为 1->2->3->5.
 *
 *  说明：
 *
 *  给定的 n 保证是有效的。
 *
 *  进阶：
 *
 *  你能尝试使用一趟扫描实现吗？
 *
 *  链接：https://leetcode-cn.com/problems/remove-nth-node-from-end-of-list
 *
 */
object RemoveNthFromEnd {
    /**
     * 解题思路：采用双指针的方法
     * 添加一个哨兵节点在头部，使用两个指针[p]、[q]
     * 1.[q]先测量出n的长度
     * 2.[p]和[q]然后一起滑动，滑动至[q]到null为止
     * 3.此时需要删除的点就为[p]的下一个节点
     * 4.然后将[p]的下一个界面设置为删除点的下一个界面，这样就把删除点从链表中删除了
     *
     */
    fun removeNthFromEnd(head:SinglyListNode?,n:Int):SinglyListNode?{
        val dumpNode = SinglyListNode(-1,head)
        var q:SinglyListNode? = dumpNode
        var p:SinglyListNode? = dumpNode
        for(index in 0 until n+1){
            q = q?.next
        }

        while (q != null){
            p = p?.next
            q = q.next
        }

        val result = p?.next
        p?.next = result?.next
        return result
    }
}

fun main(args: Array<String>) {
    var head:SinglyListNode? = generateSingleListNode()
    var removeNthFromEnd = RemoveNthFromEnd.removeNthFromEnd(head, 3)
    println("remove node ${removeNthFromEnd?.value}")
    println("node --> $head")
    removeNthFromEnd = RemoveNthFromEnd.removeNthFromEnd(head, 3)
    println("remove node ${removeNthFromEnd?.value}")
    println("node1 --> $head")

}