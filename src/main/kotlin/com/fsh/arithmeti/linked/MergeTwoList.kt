package com.fsh.arithmeti.linked

/**
 * 题目：合并两个有序链表
 *
 * 将两个升序链表合并为一个新的 升序 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。 
 *
 * 示例：
 *  输入：1->2->4, 1->3->4
 *  输出：1->1->2->3->4->4
 *
 *  链接：https://leetcode-cn.com/problems/merge-two-sorted-lists
 *
 */
object MergeTwoList{
    /**
     * 解题思路：
     *  两个列表都是排好序的，可以直接判断两个列表的节点数据大小，
     *  如果[list1]节点数据比[list2]数据小,就把[list1]节点加入结果[head],同时[list1]和[head]节点往后移动
     *  否则将[list2]节点数据加入结果[head]中,同时[list2]和[head]节点往后移动
     *  在最后判断[list1]或[list2]的最后一个节点是否为null，如果非空，加入结果
     */
    fun mergeTwoList(list1:SinglyListNode?,list2:SinglyListNode?):SinglyListNode?{
        val result:SinglyListNode? =  SinglyListNode(Int.MIN_VALUE,null)
        var head = result
        var l1 = list1
        var l2 = list2
        while (l1 != null && l2 != null){
            when{
                l1.value < l2.value ->{
                    head?.next = l1
                    l1 = l1.next
                }
                else->{
                    head?.next = l2
                    l2 = l2.next
                }
            }
            head = head?.next
        }

        if(l1 != null){
            head?.next = l1
        }
        if(l2 != null){
            head?.next = l2
        }
        return result?.next
    }
}

fun main(args: Array<String>) {
    val l1 = generateSingleListNode()
    val l2 = generateSingleListNode()
    val mergeTwoList = MergeTwoList.mergeTwoList(l1, l2)

    println("merge result --> $mergeTwoList")
}