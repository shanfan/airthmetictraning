package com.fsh.arithmeti.linked

/**
 * 单向链表
 */
class SinglyListNode(var value:Int,var next:SinglyListNode?){
    override fun toString(): String {
        if(next == null){
            return value.toString()
        }
        val ssb = StringBuffer("[")
        var tempNext:SinglyListNode? = this
        while (tempNext != null){
            ssb.append("${tempNext.value},")
            tempNext = tempNext.next
        }
        ssb.delete(ssb.length-1,ssb.length)
        ssb.append("]")
        return ssb.toString()
    }
}


fun generateSingleListNode():SinglyListNode{
    return SinglyListNode(0, SinglyListNode(
            1, SinglyListNode(2, SinglyListNode(3,
            SinglyListNode(4, SinglyListNode(5,
                    SinglyListNode(6, SinglyListNode(7,
                            SinglyListNode(8, SinglyListNode(9,null))))))))
    ))
}

fun generateCycleSinglyLinkedNode():SinglyListNode{
    val cyclePoint = SinglyListNode(1,null)
    val end = SinglyListNode(2, SinglyListNode(3, SinglyListNode(4,cyclePoint)))
    val head = SinglyListNode(0,cyclePoint)
    cyclePoint.next = end
    return head
}

fun generateSinglyNode(vararg nums:Int):SinglyListNode{
    val result = SinglyListNode(Int.MIN_VALUE,null)
    var head = result
    nums.forEachIndexed {index,value->
        head.value = value
        if(index != nums.size-1){
            head.next = SinglyListNode(Int.MIN_VALUE,null)
            head = head.next!!
        }
    }
    return result
}