# 算法练习

根据[`小浩算法`](https://www.geekxh.com/0.0.%E5%AD%A6%E4%B9%A0%E9%A1%BB%E7%9F%A5/01.html)系列文章学习算法，其中对于解题思路不清晰的描述，
直接在[`LeetCode`](https://leetcode-cn.com/problems)上面寻找的

### [数组系列](src/main/kotlin/com/fsh/arithmeti/array)

1.[两数组的交集(350)](src/main/kotlin/com/fsh/arithmeti/array/ArrayMixed.kt)

2.[最长公共前缀(14)](src/main/kotlin/com/fsh/arithmeti/array/LongestCommonPrefix.kt)

3.[买卖股票的最佳时机(122)](src/main/kotlin/com/fsh/arithmeti/array/StockMaxProfitI.kt)

4.[旋转数组(189)](src/main/kotlin/com/fsh/arithmeti/array/RotationArray.kt)

5.[原地删除(27)](src/main/kotlin/com/fsh/arithmeti/array/RemoveElement.java)

6.[加一(66)](src/main/kotlin/com/fsh/arithmeti/array/PlusOne.kt)

7.[两数之和(1)](src/main/kotlin/com/fsh/arithmeti/array/TwoNumberSum.kt)

8.[三数之和(15)](src/main/kotlin/com/fsh/arithmeti/array/ThreeNumSumZero.kt)

### [链表系列](src/main/kotlin/com/fsh/arithmeti/linked)

1.[删除链表倒数第n个节点](src/main/kotlin/com/fsh/arithmeti/linked/RemoveNthFromEnd.kt)

2.[合并两个有序链表](src/main/kotlin/com/fsh/arithmeti/linked/MergeTwoList.kt)

3.[环形链表](src/main/kotlin/com/fsh/arithmeti/linked/HasCycle.kt)

4.[两数相加](src/main/kotlin/com/fsh/arithmeti/linked/AddTwoNumbers.kt)

### [动态规划]()

1.[爬楼梯](src/main/kotlin/com/fsh/arithmeti/dp/ClimbStairs.kt)

2.[最大子序和](src/main/kotlin/com/fsh/arithmeti/dp/MaxSubArray.kt)

3.[最长上升子序列](src/main/kotlin/com/fsh/arithmeti/dp/LengthOfLIS.kt)

4.[三角形最小路径和](src/main/kotlin/com/fsh/arithmeti/dp/MinimumTotal.kt)

TODO 动态规划的内容暂时先不练习，还未掌握动态规划的思想

### [字符串系列]()

1.[反转字符串](src/main/kotlin/com/fsh/arithmeti/string/ReverseString.kt)

2.[字符串中的第一个唯一字符](src/main/kotlin/com/fsh/arithmeti/string/FirstUniqChar.kt)

3.[Sunday匹配算法](src/main/kotlin/com/fsh/arithmeti/string/Sunday.kt)

4.[打印大数](src/main/kotlin/com/fsh/arithmeti/string/PrintNumbers.kt)

//TODO 5.[KMP](src/main/kotlin/com/fsh/arithmeti/string/KMP.kt)

6.[验证回文串](src/main/kotlin/com/fsh/arithmeti/string/IsPalindrome.kt)

7.[旋转字符串](src/main/kotlin/com/fsh/arithmeti/string/RotateString.kt)

8.[最后一个单词的长度](src/main/kotlin/com/fsh/arithmeti/string/LengthOfLastWord.kt)

### [二叉树系列]()

1.[二叉树的最大深度](src/main/kotlin/com/fsh/arithmeti/btree/MaxDepth.kt)

2.[二叉树的最大深度--广度优先算法](src/main/kotlin/com/fsh/arithmeti/btree/BFSMaxDepth.kt)

3.[验证二叉搜索树](src/main/kotlin/com/fsh/arithmeti/btree/ValidBST.kt)

4.[搜索二叉树](src/main/kotlin/com/fsh/arithmeti/btree/SearchBST.kt)

5.[删除二叉搜索树中的节点](src/main/kotlin/com/fsh/arithmeti/btree/DeleteNode.java)

6.[平衡二叉树](src/main/kotlin/com/fsh/arithmeti/btree/IsBalanced.kt)

7.[完全二叉树的节点个数](src/main/kotlin/com/fsh/arithmeti/btree/FullTreeNodeCount.kt)